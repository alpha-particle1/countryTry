package cn.countryplay;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScans;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
@ComponentScans({
        @ComponentScan("cn.countryplay.*"),
})
public class CountryPlayApplication {

    public static void main(String[] args) {
        SpringApplication.run(CountryPlayApplication.class, args);
    }

}
