package cn.countryplay.mapper.commodity.impl;

import cn.countryplay.entity.commodity.Commodity;
import cn.countryplay.entity.user.UserShoppingTrolley;
import cn.countryplay.mapper.commodity.ICommodityDao;
import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.regex.Pattern;

/**
 * Author:甲粒子
 * Date: 2022/4/7
 * Description：
 */
@Repository
public class CommodityDaoImpl implements ICommodityDao {

    @Autowired
    private MongoTemplate mongo;

    @Override
    public int addCommodity(Commodity commodity) {
        if(mongo.insert(commodity)!=null){
            return 1;
        }
        return 0;
    }

    @Override
    public int offShelves(String cid) {
        Query query = new Query();
        query.addCriteria(Criteria.where("_id").is(cid));
        DeleteResult remove = mongo.remove(query, Commodity.class);
        if(remove.getDeletedCount()==0)
            return 0;
        return 1;
    }

    @Override
    public int addCommodityNums(String commodityId, int nums) {
        Query query = new Query();
        query.addCriteria(Criteria.where("commodityId").is(commodityId));
        Update updateObj = new Update();
        updateObj.inc("nums",nums);
        UpdateResult updateResult = mongo.updateFirst(query, updateObj, Commodity.class);
        if(updateResult == null || updateResult.getModifiedCount() == 0){
            return 0;
        }
        return 1;
    }

    @Override
    public int minusCommodityNums(String commodityId, int nums) {
        Query query = new Query();
        query.addCriteria(Criteria.where("_id").is(commodityId));
        Update updateObj = new Update();
        updateObj.inc("nums",nums*(-1));
        UpdateResult updateResult = mongo.updateFirst(query, updateObj, Commodity.class);
        if(updateResult == null || updateResult.getModifiedCount() == 0){
            return 0;
        }
        return 1;
    }

    @Override
    public long getCommodityNums(String commodityId) {
        Query query = new Query();
        query.addCriteria(Criteria.where("_id").is(commodityId));
        return  mongo.findOne(query, Commodity.class).getNums();
    }

    @Override
    public List<Commodity> getCommodityPage(long curPage, int pageSize) {
        Query query=new Query();
        Sort sort=Sort.by(Sort.Direction.DESC,"upTime");
        return mongo.find(query.with(sort).skip((curPage-1)*pageSize).limit(pageSize), Commodity.class);
    }

    @Override
    public List<Commodity> getCommodityPage(String key, int type, int srt, long curPage, int pageSize) {
        Criteria criteria = new Criteria();
        Pattern pattern = Pattern.compile("^.*"+key+".*$",Pattern.CASE_INSENSITIVE) ;
        Query query ;
        //key
        if (type == 0 && key.equals(">_<")){
            query =  new Query();
            //query = new Query(criteria.orOperator(Criteria.where("title").regex(pattern),Criteria.where("description").regex(pattern)));
        }else if (type != 0 && key.equals(">_<")){
            query = new Query(Criteria.where("type").is(type));
        }
        else if(type == 0 && !key.equals(">_<")){
            query = new Query(criteria.orOperator(Criteria.where("title").regex(pattern),Criteria.where("description").regex(pattern)));
        }
        else{
            query = new Query(criteria.orOperator(Criteria.where("title").regex(pattern).and("type").is(type),Criteria.where("description").regex(pattern).and("type").is(type)));
        }

        Sort sort=Sort.by(Sort.Direction.DESC,"upTime");
        switch (srt){
            case 1:{
                sort=Sort.by(Sort.Direction.DESC,"upTime");
            }break;
            case 2:{
                sort=Sort.by(Sort.Direction.ASC,"upTime");
            }break;
            case 3:{
                sort=Sort.by(Sort.Direction.DESC,"price");
            }break;
            case 4:{
                sort=Sort.by(Sort.Direction.ASC,"price");
            }break;
        }
        return mongo.find(query.with(sort).skip((curPage-1)*pageSize).limit(pageSize), Commodity.class);

    }

    @Override
    public long countCommodity() {
        return mongo.count(new Query(),Commodity.class);
    }

    @Override
    public long countCommodity(String key, int type) {
        Criteria criteria = new Criteria();
        Pattern pattern = Pattern.compile("^.*"+key+".*$",Pattern.CASE_INSENSITIVE) ;
        Query query = new Query(criteria.orOperator(Criteria.where("title").regex(pattern).and("type").is(type),Criteria.where("description").regex(pattern).and("type").is(type)));
        return mongo.count(query,Commodity.class);
    }

    @Override
    public int joinTheShoppingCart(UserShoppingTrolley shopping) {
        if(mongo.insert(shopping)!=null){
            return 1;
        }
        return 0;
    }

    @Override
    public int removeTheShoppingCart(Integer userId, String commodityId) {
        Query query = new Query();
        query.addCriteria(Criteria.where("commodityId").is(commodityId).and("userId").is(userId));
        DeleteResult remove = mongo.remove(query,UserShoppingTrolley.class);
        if(remove.getDeletedCount()==0)
            return 0;
        return 1;
    }

    @Override
    public List<Commodity> getTheShoppingCartByPage(Integer userId, long curPage, int pageSize) {
        // 构建 Aggregation：添加查询条件
        Aggregation aggregation = Aggregation.newAggregation(
                // 关联commodity集合(表)
                //查询购物车的商品号，然后查询商品信息
                Aggregation.lookup(
                        "commodity",//关联从表名
                        "commodityId",//主表关联字段
                        "_id",//从表关联的字段
                        "docs_member" //查询结果名（类似mysql的衍生表）
                ),
                Aggregation.match(
                        Criteria.where("userId").is(userId)                                                      // 店铺状态: 1:已审核
                ),
                // 分页：页码
                Aggregation.skip((curPage-1)*pageSize),
                // 分页：条数
                Aggregation.limit(pageSize),
                // 排序：加入购物车时间降序排序
                Aggregation.sort(Sort.Direction.DESC,"cTime")//加入购物车时间
        );
        // 执行查询，这里的userShoppingTrolley必须是查询的主表（集合）名
        //参数三 Commodity.class 是返回类型
        AggregationResults<Commodity> results = mongo.aggregate(aggregation, "userShoppingTrolley",Commodity.class);

        return results.getMappedResults();
    }

    @Override
    public long countTheShoppingCart(Integer userId) {
        return mongo.count(new Query(Criteria.where("userId").is(userId)),UserShoppingTrolley.class);

    }
}
