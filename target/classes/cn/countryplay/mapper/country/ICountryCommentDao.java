package cn.countryplay.mapper.country;
import cn.countryplay.entity.country.CountryComment;
import java.util.List;
/**
 * Author:甲粒子
 * Date: 2022/3/29
 * Description：乡村评论 存储使用mongoDB 为了区分mysql对应的mapper这里使用dao
 */
public interface ICountryCommentDao {
    /**
     * 增加:乡村评论
     * @param comment 评论内容
     * @return
     */
    int comment(CountryComment comment);

    /**
     * 删除评论
     * @param id 评论id
     * @return
     */
    int deleteComment(String id);


    /**
     * 获取乡村id为countryId的评论数量
     * @param countryId 乡村id
     * @return
     */
    long countComments(int countryId);

    /**
     *分页查询
     * @param countryId 乡村id
     * @return 对应id的乡村的评论页
     */
    List<CountryComment> getCommentPage(int countryId, int curPage, int pageSize);

}
