package cn.countryplay.mapper.country.impl;

import cn.countryplay.entity.restaurant.RestaurantComment;
import cn.countryplay.mapper.country.ICountryRestaurantCommentDao;
import com.mongodb.client.result.DeleteResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
/**
 * Author:甲粒子
 * Date: 2022/4/6
 * Description：
 */
@Repository
public class CountryRestaurantCommentDaoImpl implements ICountryRestaurantCommentDao {
    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public int comment(RestaurantComment comment) {
        RestaurantComment insert = mongoTemplate.insert(comment);
        if(insert==null){
            return 0;
        }
        return 1;
    }

    @Override
    public int deleteComment(String id) {
        Query query=new Query();
        query.addCriteria(Criteria.where("_id").is(id));
        DeleteResult commentDel = mongoTemplate.remove(query, "restaurantComment");
        if(commentDel.getDeletedCount()==0){
            return 0;
        }
        return 1;
    }

    @Override
    public long countComments(int countryId) {
        Query query=new Query();
        query.addCriteria(Criteria.where("restId").is(countryId));
        return mongoTemplate.count(query,RestaurantComment.class);
    }

    @Override
    public List<RestaurantComment> getCommentPage(int countryId, int curPage, int pageSize) {
        Query query=new Query();
        query.addCriteria(Criteria.where("restId").is(countryId));
        return mongoTemplate.find(query.skip((curPage-1)*pageSize).limit(pageSize),RestaurantComment.class);
    }
}
