package cn.countryplay.mapper.user;
import cn.countryplay.entity.user.*;
import org.apache.ibatis.annotations.Mapper;
import java.util.List;
/**
 * Author:甲粒子
 * Date: 2022/3/29
 * Description：用户 的增查改
 */
@Mapper
public interface UserMapper {
//------------------------------------------------用户基本信息相关---------------------------------------------------
    /**
     * 新增用户
     * @param user 新增的用户
     * @return
     */
    int newUser(User user);

    /**
     * 查询用户是否存在
     * @param openId
     * @return
     */
    int selectUser(String openId);

    /**
     * 查询用户userId
     * @param openId
     * @return
     */
    Integer selectUserId(String openId);

    /**
     * 更新用户信息:nickName
     * @param userId 用户id
     * @param nickName 用户昵称
     * @return
     */
    int setNickName(Integer userId,String nickName);

    /**
     * 更新用户信息:头像
     * @param userId 用户id
     * @param avatar 用户头像
     * @return
     */
    int setAvatar(Integer userId,String avatar);

    /**
     * 更新用户信息: phone
     * @param userId 用户id
     * @param phone phone
     * @return
     */
    int setPhone(Integer userId,String phone);

    /**
     * 更新用户信息:邮箱
     * @param userId 用户id
     * @param email
     * @return
     */
    int setEmail(Integer userId,String email);

    /**
     * 更新用户信息: 最近登录时间
     * @param userId 用户id
     * @param rTime 最近登录时间
     * @return
     */
    int setRTime(Integer userId,String rTime);

    /**
     * 更新用户信息
     * @param user 用户
     * @return
     */
    int updateUserInfo(User user);


    /**
     * 获取用户邮箱
     * @param userId 用户id
     * @return
     */
    String getUserEmail(Integer userId);

    //------------------------------------------------用户权限相关---------------------------------------------------

    /**
     * 获取用户权限
     * @param userId 用户id
     * @return
     */
    List<UserRoles> getUserRoles(Integer userId);

    /**
     * 增加用户权限
     * @param userId 用户id
     * @param roleId 权限id
     * @return
     */
    int addUserRole(Integer userId,int roleId);

    /**
     * 更改用户权限
     * @param userId 用户id
     * @param oldRoleId 旧权限id
     * @param newRoleId 新权限id
     * @return
     */
    int setUserRole(Integer userId,int oldRoleId,int newRoleId);

    /**
     * 删除用户权限
     * @param userId 用户id
     * @param roleId 权限id
     * @return
     */
    int deleteUserRole(Integer userId,int roleId);

    /**
     * 判断用户是否存在某权限
     * @param userId 用户id
     * @param roleId 权限id
     * @return
     */
    int userHaveRole(Integer userId,int roleId);

    //------------------------------------------------用户账户相关---------------------------------------------------

    /**
     * 初始化用户账户
     * @param userAccount 用户账户
     * @return 0 1
     */
    int newUserAccount(UserAccount userAccount);


    /**
     * 判断用户账户是否存在
     * @param userId 用户id
     * @return 用户账户数目
     */
    int userAccountNum(Integer userId);


    /**
     * 充值虚拟货币
     * @param userId 用户id
     * @param money 充值金额
     * @return
     */
    int recharge(Integer userId,Integer money);

    /**
     * 消费
     * @param userId 用户id
     * @param money 充值金额
     * @return
     */
    int consume(Integer userId,Integer money);

    /**
     * 余额
     * @param userId 用户id
     * @return
     */
    int balance(Integer userId);

    /**
     * 账户最近变动时间
     * @param userId 用户id
     * @param rTime 账户最近一次变动时间
     * @return
     */
    int setUserAccountRTime(Integer userId,String rTime);

    /**
     * 设置用户账户状态
     * @param userId 用户id
     * @param status 账户状态
     * @return
     */
    int setUserAccountStatus(Integer userId,int status);

    /**
     * 更新用户密码
     * @param userId 用户id
     * @param pwd 用户密码
     * @return
     */
    int setUserAccountPwd(Integer userId,String pwd);

    /**
     * 验证用户密码
     * @param userId 用户id
     * @param pwd 用户密码
     * @return
     */
    int checkUserAccountPwd(Integer userId,String pwd);

    //------------------------------------------------用户订单相关---------------------------------------------------

    /**
     * 新订单
     * @param order 用户订单
     * @return
     */
    int newUserOrder(UserOrder order);

    /**
     * 更新支付状态
     * @param orderId 订单id
     * @param status 订单状态
     * @return
     */
    int setOrderStatus(String orderId,int status);

    /**
     * 查询订单状态
     * @param orderId 订单号
     * @return
     */
    int getOrderStatus(String orderId);

    /**
     * 获取用户收货地址
     * @param userId
     * @return
     */
    UserReceivingAddress getUserReceivingAddress(Integer userId);

    /**
     * 新增收货地址
     * @param
     * @param userReceivingAddress 收货地址
     * @return
     */
    int addReceivingAddress(UserReceivingAddress userReceivingAddress);

    /**
     * 设置收货地址
     * @param
     * @param userReceivingAddress 收货地址
     * @return
     */
    int setReceivingAddress(UserReceivingAddress userReceivingAddress);

    /**
     * 分页拉取订单：用户
     * @param userId 用户id
     * @param status 订单状态
     * @param curPage
     * @param pageSize
     * @return
     */
    List<UserOrder> getUserOrders(Integer userId,int status,long curPage, int pageSize);


    /**
     * 用户订单信息条数
     * @param
     * @param
     * @return
     */
    long countUserOrders(Integer userId,int status);


    /**
     * 分页拉取订单：商户
     * @param saleId 商户id
     * @param status 订单状态
     * @param curPage
     * @param pageSize
     * @return
     */
    List<UserOrder> getUserOrdersB(Integer saleId,int status,long curPage, int pageSize);


    /**
     * 用户订单信息条数
     * @param saleId
     * @param status
     * @return
     */
    long countUserOrdersB(Integer saleId,int status);


}
