package cn.countryplay.aop;

import cn.countryplay.entity.annotation.Authentication;
import cn.countryplay.entity.user.UserRoles;
import cn.countryplay.service.user.IUserService;
import cn.countryplay.utils.JWTUtil;
import cn.countryplay.utils.RespStateUtil;
import com.auth0.jwt.interfaces.DecodedJWT;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

/**
 * Author:甲粒子
 * Date: 2022/4/2
 * Description：权限验证
 */
@Aspect
@Component
public class VerifyPermissions {
    @Autowired
    private IUserService userService;

    @Pointcut("@annotation(authentication)")
    public void doAuthenticate(Authentication authentication){

    }

    @Around("doAuthenticate(authentication)")
    public Object doAround(ProceedingJoinPoint joinPoint,Authentication authentication){
        // 获得request对象
        RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
        ServletRequestAttributes servletRequestAttributes = (ServletRequestAttributes) requestAttributes;
        HttpServletRequest request = servletRequestAttributes.getRequest();
        String token = request.getHeader("token");
        Integer userId;
        if(token==null || token.equals("")){
            return RespStateUtil.state_1(new HashMap<>());
        }
        try {
            userId = getUserIdFromToken(token);
        } catch (Throwable throwable) {
            //throwable.printStackTrace();
            return RespStateUtil.state_7(new HashMap<>());
        }
        //System.out.println(userId);
        List<UserRoles> userRoles = userService.getUserRoles(userId);

        if(userRoles != null){
            if (checkAuth(userRoles,authentication)) {
                try {
                    return joinPoint.proceed();
                } catch (Throwable throwable) {
                    return RespStateUtil.stateError(new HashMap<>());
                }
            }
            return RespStateUtil.state_2(new HashMap<>());
        }
        return RespStateUtil.stateError(new HashMap<>());
    }

    /**
     * 解析token 拿到userId
     * @return
     */
    public static Integer getUserIdFromToken(String token){
        DecodedJWT tokenPayload = JWTUtil.getTokenPayload(token);
        String role = tokenPayload.getClaim("userId").asString();
        return Integer.parseInt(role);
    }

    /**
     * 验证权限
     * @return
     */
    public static boolean checkAuth(List<UserRoles> userRoles,Authentication authentication){
        int[] role = authentication.role();
        List<Integer> role_ = new ArrayList<>();
        List<Integer> roles_ = new ArrayList<>();
        for(int i = 0 ; i < role.length ; ++i){
            role_.add(role[i]);
        }
        for (UserRoles userRole : userRoles) {
            roles_.add(userRole.getRoleId());
        }
       return !Collections.disjoint(role_, roles_);
    }


}
