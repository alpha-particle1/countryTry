package cn.countryplay.utils;
import com.alibaba.fastjson.JSONObject;
import java.util.Map;
/**
 * Author:甲粒子
 * Date: 2022/3/31
 * Description： 接口状态返回工具
 */
public class RespStateUtil {

    /**
     * 判断数据库操作是否成功
     * @param row 受影响的行数
     * @return boolean
     */
    public static boolean isSuccessfully(int row){
        if(row == 0){
            return false;
        }
        return true;
    }

    /**
     * 返回状态 state 0
     * @return
     */
    public static String state0(Map<String,Integer> map){
        map.put("state",0);
       return JSONObject.toJSONString(map);
    }

    /**
     * 返回状态 state 0
     * @return
     */
    public static String state1(Map<String,Integer> map){
        map.put("state",1);
        return JSONObject.toJSONString(map);
    }


    /**
     * 返回状态 state -1
     * @return
     */
    public static String state_1(Map<String,Object> map){
        map.put("state",-1);
        map.put("info","token is null");
        return JSONObject.toJSONString(map);
    }

    /**
     * 返回状态 state -2
     * @return
     */
    public static String state_2(Map<String,Object> map){
        map.put("state",-2);
        map.put("info","no auth!");
        return JSONObject.toJSONString(map);
    }

    /**
     * 返回状态 state -7
     * @return
     */
    public static String state_7(Map<String,Object> map){
        map.put("state",-7);
        map.put("info","token过期or无效token");
        return JSONObject.toJSONString(map);
    }

    /**
     * 返回状态 state error
     * @return
     */
    public static String stateError(Map<String,Object> map){
        map.put("state",0);
        map.put("info","error!");
        return JSONObject.toJSONString(map);
    }


}
