package cn.countryplay.utils;
/**
 * Author:甲粒子
 * Date: 2022/4/4
 * Description：id生成器
 */
public class IdUtil {

    //生成用户id
    public static Integer generateUserId(){
        long id = SnowflakeIdWorker.generateId();
        StringBuilder sb=new StringBuilder(String.valueOf(id));
        StringBuilder reverse = sb.reverse();
        id=new Long(reverse.toString())/1000;
        while(id>1999999999){
            id/=10;
        }
        Integer userId = Math.toIntExact(id);
        return userId;
    }
}
