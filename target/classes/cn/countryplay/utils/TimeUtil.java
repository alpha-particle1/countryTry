package cn.countryplay.utils;
import java.text.SimpleDateFormat;
import java.util.Date;
/**
 * Author:甲粒子
 * Date: 2022/3/31
 * Description：时间生成工具类
 */
public class TimeUtil {
    public static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

    public static String getNowTime(){
        return sdf.format(new Date());
    }
}
