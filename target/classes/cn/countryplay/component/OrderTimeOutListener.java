package cn.countryplay.component;

import cn.countryplay.entity.constantPool.RabbitConstant;
import cn.countryplay.entity.user.UserOrder;
import cn.countryplay.service.commodity.ICommodityService;
import cn.countryplay.service.user.IUserService;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Author:甲粒子
 * Date: 2022/4/7
 * Description：死信消费者监听dead.letter.order死信队列，消费过期消息
 */
@Component

public class OrderTimeOutListener {
    @Autowired
    private IUserService userService;
    @Autowired
    private ICommodityService commodityService;

    //@RabbitHandler
    @RabbitListener(queues = RabbitConstant.DEAD_LETTER_QUEUE_ORDER)//RabbitConstant.ORDER_QUEUE 是通的
    public void takeMessage(UserOrder order){
        System.out.println("过期");
       /* //通过从死信队列中获取的订单号，查询订单
        int orderStatus = userService.getOrderStatus(order.getOrderId());
        //判断订单状态如果为未支付，则修改状态为过期同时归还数量
        if (orderStatus != 1){
            userService.setOrderStatus(order.getOrderId(),-2);
            //归还数量
            commodityService.addCommodityNums(order.getCommodityId(),order.getNums());
        }*/
    }

}
