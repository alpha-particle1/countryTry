package cn.countryplay.component;

import cn.countryplay.entity.constantPool.RabbitConstant;
import cn.countryplay.entity.mail.MailVo;
import cn.countryplay.service.mail.MailService;
import cn.countryplay.utils.redis.RedisUtil;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.Random;

/**
 * Author:甲粒子
 * Date: 2022/4/6
 * Description：
 */
@Component
@RabbitListener(queues = RabbitConstant.EMAIL_QUEUE)
public class TopicEmailReceiver {
    @Autowired
    private RedisUtil redis;
    @Autowired
    private MailService mailService;
    @RabbitHandler
    public void getMessageAndSendEmail(Map msg){
        Integer userId = (Integer) msg.get("userId");
        String userEmail = (String) msg.get("email");
        //用用户id+一个后缀_code 存储在redis中，过期时间为15分钟
        String code = verificationCode();
        redis.set(userId+"_code",code,900);
        //发送邮件
        MailVo vo=new MailVo();
        vo.setTo(userEmail);
        vo.setSubject("【乡村旅游小程序】");
        vo.setText("【乡村旅游小程序】尊敬的用户您正在进行支付密码更改，验证码是" +
                code +
                ",【15分钟内有效】请勿将验证码泄露给他人,如非本人操作，请不与理会！");
        mailService.sendTextMail(vo);

        System.out.println("发送消息: "+vo.toString()+" 成功");
    }

    public static String verificationCode() {
        //生成六位随机正整数
        Random random = new Random();
        String verificationCode = String.valueOf(random.nextInt(9) + 1);
        for (int i = 0; i < 5; i++) {
            verificationCode += random.nextInt(10);
        }
        return verificationCode;
    }

}
