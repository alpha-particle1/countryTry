package cn.countryplay.service.admin.impl;
import cn.countryplay.entity.admin.CountryAdmin;
import cn.countryplay.entity.admin.RoleTable;
import cn.countryplay.mapper.admin.AdminMapper;
import cn.countryplay.mapper.admin.RoleMapper;
import cn.countryplay.service.admin.IAdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Author:甲粒子
 * Date: 2022/3/24
 * Description：
 */
@Service
public class AdminServiceImpl implements IAdminService {

    @Autowired
    //@Qualifier(value="adminMapper")
    private AdminMapper adminMapper;
    @Autowired
    private RoleMapper roleMapper;
    //加密工具类
    BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        try {
            CountryAdmin admin =adminMapper.loadAdminByAdminName(username);
            if(admin==null) {
                throw new UsernameNotFoundException("用户名错误，或不存在!");
                //return null;
            }
            List<SimpleGrantedAuthority> authorities = new ArrayList<>();
            List<RoleTable> list = roleMapper.findRoleByAdminId(admin.getId());
            for(RoleTable role : list) {
                authorities.add(new SimpleGrantedAuthority(role.getName()));
            }
            //封装 SpringSecurity  需要的UserDetails 对象并返回
            // 加密： 在这里加密是因为在数据库插值的时候 是插入的明文：如 “123456”
            //SpringSecurity要求读取的是加密的密码
            //不然报错
            //所以在这里模拟注册时候的加密
            String encodedPassword = passwordEncoder.encode(admin.getUserpwd().trim());

            admin.setUserpwd(encodedPassword);
            for (SimpleGrantedAuthority authority : authorities) {
                System.out.println(authority.getAuthority());
            }
            UserDetails userDetails = new User(admin.getUsername(),admin.getUserpwd(),authorities);
            return userDetails;
        } catch (Exception e) {
            e.printStackTrace();
            //返回null即表示认证失败
            throw new UsernameNotFoundException("错误,授权失败!");
            //return null;
        }
    }
}
