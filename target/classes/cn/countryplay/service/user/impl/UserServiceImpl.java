package cn.countryplay.service.user.impl;
import cn.countryplay.entity.constantPool.Constant;
import cn.countryplay.entity.user.*;
import cn.countryplay.mapper.user.UserMapper;
import cn.countryplay.service.user.IUserService;
import cn.countryplay.utils.AesCbcUtil;
import cn.countryplay.utils.HttpClientUtil;
import cn.countryplay.utils.RespStateUtil;
import cn.countryplay.utils.TimeUtil;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.Map;

/**
 * Author:甲粒子
 * Date: 2022/3/30
 * Description：
 */
@Service
public class UserServiceImpl implements IUserService {
    @Autowired
    private UserMapper userMapper;

    @Override
    public int newUser(User user) {
        return userMapper.newUser(user);
    }

    @Override
    public int selectUser(String openId) {
        return userMapper.selectUser(openId);
    }

    @Override
    public Integer selectUserId(String openId) {
        return userMapper.selectUserId(openId);
    }

    @Override
    public int setNickName(Integer userId, String nickName) {
        return userMapper.setNickName(userId,nickName);
    }

    @Override
    public int setAvatar(Integer userId, String avatar) {
        return userMapper.setAvatar(userId,avatar);
    }

    @Override
    public int setPhone(Integer userId, String phone) {
        return userMapper.setPhone(userId,phone);
    }

    @Override
    public int setEmail(Integer userId, String email) {
        return userMapper.setEmail(userId,email);
    }

    @Override
    public int setRTime(Integer userId, String rTime) {
        return userMapper.setRTime(userId,rTime);
    }

    @Override
    public int updateUserInfo(User user) {
        return userMapper.updateUserInfo(user);
    }

    @Override
    public String getUserEmail(Integer userId) {
        return userMapper.getUserEmail(userId);
    }

    @Override
    public List<UserRoles> getUserRoles(Integer userId) {
        return userMapper.getUserRoles(userId);
    }

    @Override
    public int addUserRole(Integer userId, int roleId) {
        return userMapper.addUserRole(userId,roleId);
    }

    @Override
    public int setUserRole(Integer userId, int oldRoleId, int newRoleId) {
        return userMapper.setUserRole(userId,oldRoleId,newRoleId);
    }

    @Override
    public int deleteUserRole(Integer userId, int roleId) {
        return userMapper.deleteUserRole(userId,roleId);
    }

    @Override
    public int userHaveRole(Integer userId, int roleId) {
        return userMapper.userHaveRole(userId,roleId);
    }

    @Override
    public int newUserAccount(UserAccount userAccount) {
        return userMapper.newUserAccount(userAccount);
    }

    @Override
    public int userAccountNum(Integer userId) {
        return userMapper.userAccountNum(userId);
    }

    @Override
    public int recharge(Integer userId, Integer money) {
        return userMapper.recharge(userId,money);
    }

    @Override
    public int consume(Integer userId, Integer money) {
        return userMapper.consume(userId,money);
    }

    @Override
    public int balance(Integer userId) {
        return userMapper.balance(userId);
    }

    @Override
    public int setUserAccountRTime(Integer userId, String rTime) {
        return userMapper.setUserAccountRTime(userId,rTime);
    }

    @Override
    public int setUserAccountStatus(Integer userId, int status) {
        return userMapper.setUserAccountStatus(userId,status);
    }

    @Override
    public int setUserAccountPwd(Integer userId, String pwd) {
        return userMapper.setUserAccountPwd(userId,pwd);
    }

    @Override
    public int checkUserAccountPwd(Integer userId, String pwd) {
        return userMapper.checkUserAccountPwd(userId,pwd);
    }

    @Override
    public int newUserOrder(UserOrder order) {
        return userMapper.newUserOrder(order);
    }

    @Override
    public int setOrderStatus(String orderId, int status) {
        return userMapper.setOrderStatus(orderId,status);
    }

    @Override
    public int getOrderStatus(String orderId) {
        return userMapper.getOrderStatus(orderId);
    }

    @Override
    public UserReceivingAddress getUserReceivingAddress(Integer userId) {
        return userMapper.getUserReceivingAddress(userId);
    }

    @Override
    public int addReceivingAddress(UserReceivingAddress userReceivingAddress) {
        return userMapper.addReceivingAddress(userReceivingAddress);
    }

    @Override
    public int setReceivingAddress(UserReceivingAddress userReceivingAddress) {
        return userMapper.setReceivingAddress(userReceivingAddress);
    }

    @Override
    public List<UserOrder> getUserOrders(Integer userId, int status, long curPage, int pageSize) {
        return userMapper.getUserOrders(userId,status,curPage,pageSize);
    }

    @Override
    public long countUserOrders(Integer userId, int status) {
        return userMapper.countUserOrders(userId,status);
    }

    @Override
    public List<UserOrder> getUserOrdersB(Integer saleId, int status, long curPage, int pageSize) {
        return userMapper.getUserOrdersB(saleId,status,curPage,pageSize);
    }

    @Override
    public long countUserOrdersB(Integer saleId, int status) {
        return userMapper.countUserOrdersB(saleId,status);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public int openUserAccount(UserAccount userAccount) {
        if(userAccount == null)return 0;
        if(userMapper.userAccountNum(userAccount.getUserId()) >= 1){
            return 0;
        }
        return userMapper.newUserAccount(userAccount);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public int newUserAndOpenAccount(User user, UserAccount userAccount) {
        if(user == null)return 0;
        if (RespStateUtil.isSuccessfully(newUser(user))) {
            addUserRole(user.getUserId(),1);
            userAccount.setUserId(user.getUserId());
            userAccount.setMoney(100);
            userAccount.setPwd("666666");
            userAccount.setCTime(TimeUtil.getNowTime());
            if (RespStateUtil.isSuccessfully(openUserAccount(userAccount))) {
                return 1;
            }
        }
        return 0;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public int checkAndPay(Integer userId, String pwd, int money,String orderId) {
        try {
            //验证密码
            int i = checkUserAccountPwd(userId, pwd);
            //验证余额
            int balance = balance(userId);
            if(balance<money){
                return -1;
            }
            //扣款
            consume(userId,money);
            //修改订单状态
            setOrderStatus(orderId,1);
            return 1;
        }catch (Exception e){
            return 0;
        }
    }


    @Override
    public String requestWX(String data, ObjectNode node, Map<String, String> params) {
        try {
            params.put("appid", Constant.APP_ID);
            params.put("secret",Constant.APP_SECRET);
            params.put("js_code",node.get("code").toString().replace("\"", ""));
            params.put("grant_type","authorization_code");
            /**
             * 向微信服务器发送请求
             */
            return HttpClientUtil.doGet(Constant.REQUEST_FOR_WX,params);
        } catch ( KeyStoreException | NoSuchAlgorithmException | KeyManagementException e ) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public ObjectNode resolveData(String data) {
        try {
            return new ObjectMapper().readValue(data, ObjectNode.class);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public ObjectNode resolveEncryptedData(ObjectNode data,ObjectNode node) {
        try {
            String decrypt = AesCbcUtil.decrypt(data.get("encryptedData").toString(), node.get("session_key").toString(), data.get("iv").toString(), "utf-8");
            return new ObjectMapper().readValue(decrypt, ObjectNode.class);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
