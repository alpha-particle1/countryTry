package cn.countryplay.service.news.impl;

import cn.countryplay.entity.news.News;
import cn.countryplay.mapper.news.ICountryNewsMapper;
import cn.countryplay.service.news.ICountryNewsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Author:甲粒子
 * Date: 2022/3/22
 * Description：
 */
@Service
public class CountryNewsServiceImpl implements ICountryNewsService {
    @Autowired
    private ICountryNewsMapper countryNewsMapper;
    @Override
    public boolean addNewsfromSprider(News news) {
        return countryNewsMapper.addNewsfromSprider(news);
    }

    @Override
    public boolean existedNews(String title) {
        return countryNewsMapper.existedNews(title);
    }

    @Override
    public long countNews() {
        return countryNewsMapper.countNews();
    }

    @Override
    public List<News> selectNewsPage(long curPage, int pageSize) {
        return countryNewsMapper.selectNewsPage(curPage,pageSize);
    }
}
