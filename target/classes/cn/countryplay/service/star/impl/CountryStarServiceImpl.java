package cn.countryplay.service.star.impl;

import cn.countryplay.entity.star.CountryStar;
import cn.countryplay.mapper.star.ICountryStarDao;
import cn.countryplay.service.star.ICountryStarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Author:甲粒子
 * Date: 2022/4/9
 * Description：
 */
@Service
public class CountryStarServiceImpl implements ICountryStarService {
    @Autowired
    private ICountryStarDao countryStarDao;
    @Override
    public int starCountry(CountryStar countryStar) {
        return countryStarDao.starCountry(countryStar);
    }

    @Override
    public int starCountry(int countryId, int score) {
        return countryStarDao.starCountry(countryId,score);
    }

    @Override
    public float getCountryStar(int countryId) {
        return countryStarDao.getCountryStar(countryId);
    }
}
