package cn.countryplay.service.city;
import cn.countryplay.entity.city.City;
import java.util.List;
/**
 * Author:甲粒子
 * Date: 2022/3/27
 * Description：城市相关服务层
 */
public interface ICityService {
    /**
     * 分页获取城市信息
     * @param curPage 当前页
     * @param pageSize 页面大小
     * @return
     */
    List<City> getCities(long curPage, int pageSize);

    /**
     * 分页获取热搜城市信息:按喜欢量倒序排序
     * @param curPage 当前页
     * @param pageSize 页面大小
     * @return
     */
    List<City> getHotCities(long curPage, int pageSize);

    /**
     * 分页获取热搜前25的城市信息
     * @return
     */
    List<City> getTop25Cities();

    /**
     * 获取城市数量，用于分页
     * @return
     */
    long countCities();

    /**
     * 喜欢人数加1
     * @param cityId 城市id
     * @return
     */
    int incLikeNums(int cityId);

    /**
     * 设置背景图片
     * @param cityId 城市id
     * @param images 城市背景图
     * @return
     */
    int setBgImages(int cityId,String images);

    /**
     * 设置城市名
     * @param cityId 城市id
     * @param name 城市名
     * @return
     */
    int setCityName(int cityId,String name);

    /**
     * 设置省份
     * @param cityId 城市id
     * @param province 城市背景图
     * @return
     */
    int setCityProvince(int cityId,String province);

    /**
     * 添加城市
     * @param city 添加的城市信息
     * @return
     */
    int insertCity(City city);
}
