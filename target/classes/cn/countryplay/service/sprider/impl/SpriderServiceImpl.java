package cn.countryplay.service.sprider.impl;
import cn.countryplay.entity.news.News;
import cn.countryplay.service.sprider.ISpriderService;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Author:甲粒子
 * Date: 2022/3/21
 * Description：
 */
@Service
public class SpriderServiceImpl implements ISpriderService {
    @Override
    public List<News> takeNewfromCTD(String url) {
        url = CHINA_TRAVEL_DOMAIN;
        List<News> news= new ArrayList<>();
        List<String> news_urls=new ArrayList<>();
        try {
            Document document = Jsoup.connect(url).get();
            Elements select = document.select("div[class^=formMiddleContent formMiddleContent597 formTabMiddleContent fk-formContentOtherPadding] " +
                    "div[class^=m_news_list m_news_col_1 head_style_0] " +
                    "div[class^=m_news_content] " +
                    "div[class^=m_news_info] " +
                    "div[class^=news_title] a");
            int size = select.size();
            for (int i = 0; i < size; i++) {
                Element element = select.get(i);
                String href = element.attr("href");
                if(!href.contains("http://")){
                   href =  href.replace("/","http://www.cntour.cn/");
                }
               news_urls.add(href);
            }


            for (String news_url : news_urls) {
                Document doc = Jsoup.connect(news_url).get();
                if(doc != null){
                    Element h1 = doc.select("h1[class^=title]").get(0);
                    String title = h1.text();
                    String time = doc.select("div[class^=leftInfo] span").get(0).text();
                    String author = doc.select("div[class^=leftInfo] span").get(1).text();
                    Elements p = doc.select("div[class^=jz_fix_ue_img] p");
                    int size1 = p.size();
                    String article="";
                    String imgs = "";
                    for (int i = 0; i < size1-2; i++) {
                        Element element = p.get(i);

                        Elements span = element.select("span");
                        if(span.size()>0){
                            article += span.text();
                        }else {
                            article += element.text();
                        }
                        Elements img = element.select("img");
                        if (img.size()>0){
                            //System.out.println(img);
                            String src = img.attr("src");
                            if(!src.contains("https://")){
                                src = src.replace("//","https://");
                            }
                            imgs += (src+"|");
                        }
                    }
                    News temp = new News();
                    temp.setNews_title(title);
                    temp.setNews_ctime(time.replace("发表时间：",""));
                    temp.setNews_author(author);
                    temp.setNews_article(article);
                    temp.setNews_imgs(imgs);
                    news.add(temp);
//                    System.out.println(title+"\n"+time+"\n"+author);
//                    System.out.println(article);
//                    System.out.println(imgs);
                }

            }
            return news;

        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            return news;
        }
    }
}
