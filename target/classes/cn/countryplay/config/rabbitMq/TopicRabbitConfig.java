package cn.countryplay.config.rabbitMq;

import cn.countryplay.entity.constantPool.RabbitConstant;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Author:甲粒子
 * Date: 2022/4/6
 * Description： topic交换机的配置
 *
 * 消费者单纯的使用，其实可以不用添加这个配置，
 * 直接建后面的监听就好，使用注解来让监听器监听对应的队列即可。
 * 配置上了的话，其实消费者也可以是生成者的身份，也能推送消息。
 */
@Configuration
public class TopicRabbitConfig {

    //邮件队列
    //true表示持久化该队列
    @Bean
    public Queue emailQueue() {
        return new Queue(RabbitConstant.EMAIL_QUEUE, true);

    }

    //话题交换机
    @Bean
    TopicExchange topicExchange() {
        return new TopicExchange(RabbitConstant.TOPIC_EXCHANGE);
    }

    //将emailQueue和topicExchange绑定,而且绑定的键值为email.key
    //这样只要是消息携带的路由键是email.key,才会分发到该emailQueue队列
    @Bean
    Binding bindingTopicExchangeOnEmailQueue() {
        return BindingBuilder.bind(emailQueue()).to(topicExchange()).with(RabbitConstant.EMAIL_ROUTING_KEY);
    }

/*    //将secondQueue和topicExchange绑定,而且绑定的键值为用上通配路由键规则topic.#
    // 这样只要是消息携带的路由键是以topic.开头,都会分发到该队列
    @Bean
    Binding bindingExchangeMessage2() {
        return BindingBuilder.bind(secondQueue()).to(exchange()).with("topic.#");
    }*/
}
