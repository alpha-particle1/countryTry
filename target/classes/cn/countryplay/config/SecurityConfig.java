package cn.countryplay.config;

import cn.countryplay.service.admin.impl.AdminServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * Author:甲粒子
 * Date: 2022/3/24
 * Description：SpringSecurity
 */
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    @Autowired
    private AdminServiceImpl adminService;
    private BCryptPasswordEncoder bCryptPasswordEncoder=new BCryptPasswordEncoder();
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.cors();


        http.formLogin().loginPage("/login")
                .usernameParameter("userName")
                .passwordParameter("userPwd")
                .loginProcessingUrl("/requestLogin")
                .failureUrl("/login")
                .successForwardUrl("/countryplay/main")
                .and().sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.IF_REQUIRED)//IF_REQUIRED  STATELESS不主动创建Session
                .invalidSessionUrl("/login")
                .maximumSessions(1);//只能有一个浏览器在线
        //如果同时登录：This session has been expired (possibly due to multiple concurrent logins being attempted as the same user).

        //如果让logout在GET请求下生效，必须关闭防止CSRF攻击csrf().disable()。
        // 如果开启了CSRF，必须使用 post方式请求/logout
        http.csrf().disable();
        //支持iframe
        http.headers().frameOptions().disable();
        http.logout().logoutSuccessUrl("/login").invalidateHttpSession(true);
        http.exceptionHandling().accessDeniedPage("/403");
    }
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(adminService).passwordEncoder(bCryptPasswordEncoder);
    }
}
