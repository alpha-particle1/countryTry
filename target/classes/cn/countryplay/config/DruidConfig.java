package cn.countryplay.config;

import com.alibaba.druid.pool.DruidDataSource;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

/**
 * Author:甲粒子
 * Date: 2022/3/24
 * Description：DruidConfig
 */
@Configuration
public class DruidConfig {
    //配置druid的datasource
    @Bean //注册到spring中
    @ConfigurationProperties(prefix = "spring.datasource")//与application.yaml中配置绑定
    public DataSource druidDataSource(){
        return new DruidDataSource();
    }
    //后台监控：  相当于web.xml  ServletRegistrationBean
    //因为SpringBoot 内置了Servlet容器所以没有web.xml  替代方法就是：ServletRegistrationBean

}
