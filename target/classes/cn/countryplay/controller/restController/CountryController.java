package cn.countryplay.controller.restController;
import cn.countryplay.entity.annotation.Authentication;
import cn.countryplay.entity.city.City;
import cn.countryplay.entity.country.Country;
import cn.countryplay.entity.page.PageObject;
import cn.countryplay.service.country.ICountryService;
import cn.countryplay.utils.QiniuUtil;
import cn.countryplay.utils.RespStateUtil;
import com.alibaba.fastjson.JSONObject;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
/**
 * Author:甲粒子
 * Date: 2022/4/2
 * Description：
 */
@Api(value = "乡村接口")
@RestController
public class CountryController {

    public static final int PAGE_SIZE = 10;

    @Autowired
    private ICountryService countryService;


    @ApiOperation(value = "城市下的热门乡村分页获取", notes = "城市下的热门乡村分页获取")
    @ResponseBody
    @GetMapping(value = "/api/countryInfoOfCity/{curPage}/{cityId}",produces = "application/json;charset=UTF-8")
    @Authentication
    public String getCountryInfoOfCity(@PathVariable long curPage,@PathVariable int cityId){
        try{
            PageObject<City> page = new PageObject<>();
            page.setCurrentpage(curPage);
            page.setTotalcount(countryService.countCountriesOfCity(cityId));
            page.setPagesize(PAGE_SIZE);
            page.setObjects(countryService.getHotCountriesOfCity(curPage*PAGE_SIZE, PAGE_SIZE,cityId));
            return JSONObject.toJSONString(page);
        }catch (Exception e){
            return RespStateUtil.state0(new HashMap<>());
        }
    }


    @ApiOperation(value = "所有乡村分页获取", notes = "所有乡村分页获取")
    @ResponseBody
    @GetMapping(value = "/api/countryInfo/{curPage}",produces = "application/json;charset=UTF-8")
    @Authentication
    public String getCountryInfo(@PathVariable long curPage){
        try{
            PageObject<City> page = new PageObject<>();
            page.setCurrentpage(curPage);
            page.setTotalcount(countryService.countCountries());
            page.setPagesize(PAGE_SIZE);
            page.setObjects(countryService.getCountries(curPage*PAGE_SIZE, PAGE_SIZE));
            return JSONObject.toJSONString(page);
        }catch (Exception e){
            return RespStateUtil.state0(new HashMap<>());
        }
    }

    @ApiOperation(value = "所有乡村热门分页获取", notes = "所有乡村热门分页获取")
    @ResponseBody
    @GetMapping(value = "/api/hotCountryInfo/{curPage}",produces = "application/json;charset=UTF-8")
    @Authentication
    public String getHotCountryInfo(@PathVariable long curPage){
        try{
            PageObject<City> page = new PageObject<>();
            page.setCurrentpage(curPage);
            page.setTotalcount(countryService.countCountries());
            page.setPagesize(PAGE_SIZE);
            page.setObjects(countryService.getHotCountries(curPage*PAGE_SIZE, PAGE_SIZE));
            return JSONObject.toJSONString(page);
        }catch (Exception e){
            return RespStateUtil.state0(new HashMap<>());
        }
    }

    @ApiOperation(value = "添加乡村信息", notes = "添加乡村信息：注意Country对象中必须要有城市id")
    @ResponseBody
    @PostMapping("/api/addCountry")
    @Authentication
    public String addCountryInfo(@RequestBody Country country, @RequestBody int nums, HttpServletRequest request){
        try{
            if(country != null){
                //上传文件
                MultipartHttpServletRequest req=(MultipartHttpServletRequest)request;
                List<MultipartFile> files = req.getFiles("imgs");
                Map<String, Object> qiniu = QiniuUtil.qiniu(files);
                if((int)qiniu.get("state")==0 || (int)qiniu.get("nums")!=nums){
                    return  RespStateUtil.state0(new HashMap<>());
                }
                String imgs = (String)qiniu.get("imgs");
                country.setImages(imgs);
                if (RespStateUtil.isSuccessfully(countryService.insertCountry(country))) {
                    return  RespStateUtil.state1(new HashMap<>());
                }
                return  RespStateUtil.state0(new HashMap<>());
            }
            return RespStateUtil.state0(new HashMap<>());
        }catch (Exception e){
            e.printStackTrace();
            return RespStateUtil.state0(new HashMap<>());
        }
    }

    @ApiOperation(value = "获取top25乡村信息", notes = "获取top25乡村信息")
    @ResponseBody
    @GetMapping(value = "/api/top25countryInfo/",produces = "application/json;charset=UTF-8")
    @Authentication
    public String getTop25CountryInfo(){
        try{
            return JSONObject.toJSONString(countryService.getTop25Countries());
        }catch (Exception e){
            return RespStateUtil.state0(new HashMap<>());
        }
    }

    @ApiOperation(value = "修改乡村背景图", notes = "修改乡村背景图【用于后台】")
    @ResponseBody
    @PostMapping("/api/updateCountryBg")
    @Authentication
    public String updateCityBg(@RequestBody String images,@RequestBody int countryId){
        if(images != null){
            if (RespStateUtil.isSuccessfully( countryService.setBgImages(countryId,images))) {
                return RespStateUtil.state1(new HashMap<>());
            }
            return RespStateUtil.state0(new HashMap<>());
        }
        return RespStateUtil.state0(new HashMap<>());
    }

}
