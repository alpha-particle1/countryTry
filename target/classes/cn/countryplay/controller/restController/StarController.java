package cn.countryplay.controller.restController;

import cn.countryplay.entity.annotation.Authentication;
import cn.countryplay.entity.user.UserOrder;
import cn.countryplay.service.star.ICountryStarService;
import cn.countryplay.utils.RespStateUtil;
import com.alibaba.fastjson.JSONObject;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
 * Author:甲粒子
 * Date: 2022/4/9
 * Description：评星
 */
@Api(value = "评星")
@RestController
public class StarController {
    @Autowired
    private ICountryStarService countryStarService;

    @ApiOperation(value = "乡村评星", notes = "乡村评星")
    @ResponseBody
    @PostMapping(value = "/api/star/country/{countryId}/{score}", produces = "application/json;charset=UTF-8")
    @Authentication
    public String submitOrder(@PathVariable int countryId,@PathVariable int score){
        try{
            countryStarService.starCountry(countryId,score);
            return RespStateUtil.state1(new HashMap<>());
        }catch (Exception e){
            e.printStackTrace();
            return RespStateUtil.state0(new HashMap<>());
        }
    }

    @ApiOperation(value = "得到乡村评星", notes = "得到乡村评星")
    @ResponseBody
    @GetMapping(value = "/api/getStar/country/{countryId}", produces = "application/json;charset=UTF-8")
    @Authentication
    public String submitOrder(@PathVariable int countryId){
        try{
            Map<String,Object> map = new HashMap<>();

            map.put("star",countryStarService.getCountryStar(countryId));
            map.put("state",1);
            return JSONObject.toJSONString(map);
        }catch (Exception e){
            e.printStackTrace();
            return RespStateUtil.state0(new HashMap<>());
        }
    }
}
