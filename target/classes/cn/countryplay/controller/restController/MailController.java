package cn.countryplay.controller.restController;

import cn.countryplay.entity.mail.MailVo;
import cn.countryplay.service.mail.MailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
/**
 * Author:甲粒子
 * Date: 2021/11/11
 * Description：测试：发送邮件
 */
@RestController
public class MailController {

    @Autowired
    private MailService mailService;


    /**
     * 测试：发送纯文本邮件
     */
    //@GetMapping("/send/txt")
    public void sendTextMail() {
        MailVo vo=new MailVo();
        vo.setTo("2285096803@qq.com");
        vo.setSubject("狂码管理密码修改验证码");
        vo.setText("请务必不要泄露此次验证码:123456");
        mailService.sendTextMail(vo);
    }

    /**
     * 发送 html 邮件
     */
   // @PostMapping("/send/html")
    public void sendHtmlMail(@RequestBody MailVo vo){
        mailService.sendHtmlMail(vo);
    }

    /**
     *  发送待附件的邮件
     */
    //@PostMapping("/send/attach")
    public void sendAttachmentsMail(@RequestBody MailVo vo){
        mailService.sendAttachmentsMail(vo);
    }

    /**
     *  发送文本中有静态资源（图片）的邮件
     */
    //@PostMapping("/send/line")
    public void sendInlineResourceMail(@RequestBody MailVo vo){
        mailService.sendInlineResourceMail(vo);
    }
}

