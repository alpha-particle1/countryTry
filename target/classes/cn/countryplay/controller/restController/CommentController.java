package cn.countryplay.controller.restController;

import cn.countryplay.entity.annotation.Authentication;
import cn.countryplay.entity.country.CountryComment;
import cn.countryplay.entity.hotel.HotelComment;
import cn.countryplay.entity.page.PageObject;
import cn.countryplay.entity.restaurant.RestaurantComment;
import cn.countryplay.service.country.ICountryCommentService;
import cn.countryplay.service.country.ICountryHotelCommentService;
import cn.countryplay.service.country.ICountryRestaurantCommentService;
import cn.countryplay.utils.RespStateUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.HashMap;
import java.util.List;
/**
 * Author:甲粒子
 * Date: 2022/4/6
 * Description：评论
 */
@Api(value = "commentAbout")
@RestController
public class CommentController {
    @Autowired
    private ICountryCommentService countryCommentService;
    @Autowired
    private ICountryHotelCommentService countryHotelCommentService;
    @Autowired
    private ICountryRestaurantCommentService countryRestaurantCommentService;

    private static final int PAGE_SIZE=16;


    @ApiOperation(value = "评论城市", notes = "评论城市")
    @Authentication
    @ResponseBody
    @RequestMapping(value = "/api/commentCountry/",method = RequestMethod.POST)
    public String comment(@RequestBody CountryComment comment){
        try {
            if(comment!=null){
                countryCommentService.comment(comment);
                return RespStateUtil.state1(new HashMap<>());
            }
            return RespStateUtil.state0(new HashMap<>());
        }catch (Exception e){
            return RespStateUtil.state0(new HashMap<>());
        }
    }

    @ApiOperation(value = "分页拉取城市评论", notes = "分页拉取城市评论")
    @Authentication
    @ResponseBody
    @RequestMapping(value = "/api/comments/country/{countryId}/{curPage}",method = RequestMethod.POST)
    public Object commentPage(@PathVariable int countryId,@PathVariable int curPage){
        try{
            PageObject<CountryComment> commentPageObject = new PageObject<>();
            commentPageObject.setCurrentpage(curPage);
            commentPageObject.setTotalcount(countryCommentService.countComments(countryId));
            commentPageObject.setPagesize(PAGE_SIZE);
            List<CountryComment> comments = countryCommentService.getCountryCommentPage(countryId,curPage,PAGE_SIZE);
            commentPageObject.setObjects(comments);
            return commentPageObject;
        }catch (Exception e){
            return RespStateUtil.state0(new HashMap<>());
        }
    }


    @ApiOperation(value = "删除城市评论", notes = "删除城市评论(管理员)")
    @Authentication
    @ResponseBody
    @PostMapping("/api/deleteComment/country/{id}")
    public Object deleteComment(@PathVariable String id){
        try{
            countryCommentService.deleteComment(id);
            return RespStateUtil.state1(new HashMap<>());
        }catch (Exception e){
            return RespStateUtil.state0(new HashMap<>());
        }
    }

    @ApiOperation(value = "评论乡村酒店", notes = "评论乡村酒店")
    @Authentication
    @ResponseBody
    @RequestMapping(value = "/api/commentHotel/",method = RequestMethod.POST)
    public String comment(@RequestBody HotelComment comment){
        try {
            if(comment!=null){
                countryHotelCommentService.comment(comment);
                return RespStateUtil.state1(new HashMap<>());
            }
            return RespStateUtil.state0(new HashMap<>());
        }catch (Exception e){
            return RespStateUtil.state0(new HashMap<>());
        }
    }

    @ApiOperation(value = "分页拉取酒店评论", notes = "分页拉取酒店评论")
    @Authentication
    @ResponseBody
    @RequestMapping(value = "/api/comments/hotel/{hotelId}/{curPage}",method = RequestMethod.POST)
    public Object hotelCommentPage(@PathVariable int hotelId,@PathVariable int curPage){
        try{

            PageObject<HotelComment> commentPageObject = new PageObject<>();
            commentPageObject.setCurrentpage(curPage);
            commentPageObject.setTotalcount(countryHotelCommentService.countComments(hotelId));
            commentPageObject.setPagesize(PAGE_SIZE);
            List<HotelComment> comments = countryHotelCommentService.getCommentPage(hotelId,curPage,PAGE_SIZE);
            commentPageObject.setObjects(comments);
            return commentPageObject;
        }catch (Exception e){
            return RespStateUtil.state0(new HashMap<>());
        }

    }


    @ApiOperation(value = "删除酒店评论", notes = "删除酒店评论(管理员)")
    @Authentication
    @ResponseBody
    @PostMapping("/api/deleteComment/hotel/{id}")
    public Object deleteHotelComment(@PathVariable String id){
        try{
            countryHotelCommentService.deleteComment(id);
            return RespStateUtil.state1(new HashMap<>());
        }catch (Exception e){
            return RespStateUtil.state0(new HashMap<>());
        }
    }

    @ApiOperation(value = "评论乡村餐馆", notes = "评论乡村餐馆")
    @Authentication
    @ResponseBody
    @RequestMapping(value = "/api/commentRestaurant/",method = RequestMethod.POST)
    public String comment(@RequestBody RestaurantComment comment){
        try {
            if(comment!=null){
                countryRestaurantCommentService.comment(comment);
                return RespStateUtil.state1(new HashMap<>());
            }
            return RespStateUtil.state0(new HashMap<>());
        }catch (Exception e){
            return RespStateUtil.state0(new HashMap<>());
        }
    }

    @ApiOperation(value = "分页拉取餐馆评论", notes = "分页拉取餐馆评论")
    @Authentication
    @ResponseBody
    @RequestMapping(value = "/api/comments/restaurant/{restId}/{curPage}",method = RequestMethod.POST)
    public Object restaurantCommentPage(@PathVariable int restId,@PathVariable int curPage){
        try{
            PageObject<RestaurantComment> commentPageObject = new PageObject<>();
            commentPageObject.setCurrentpage(curPage);
            commentPageObject.setTotalcount(countryRestaurantCommentService.countComments(restId));
            commentPageObject.setPagesize(PAGE_SIZE);
            List<RestaurantComment> comments = countryRestaurantCommentService.getCommentPage(restId,curPage,PAGE_SIZE);
            commentPageObject.setObjects(comments);
            return commentPageObject;
        }catch (Exception e){
            return RespStateUtil.state0(new HashMap<>());
        }
    }


    @ApiOperation(value = "删除餐馆评论", notes = "删除餐馆评论(管理员)")
    @Authentication
    @ResponseBody
    @PostMapping("/api/deleteComment/restaurant/{id}")
    public Object deleteRestaurantComment(@PathVariable String id){
        try{
            countryRestaurantCommentService.deleteComment(id);
            return RespStateUtil.state1(new HashMap<>());
        }catch (Exception e){
            return RespStateUtil.state0(new HashMap<>());
        }
    }



}
