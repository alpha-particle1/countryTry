package cn.countryplay.controller.restController;
import cn.countryplay.entity.annotation.Authentication;
import cn.countryplay.entity.city.City;
import cn.countryplay.entity.page.PageObject;
import cn.countryplay.service.city.ICityService;
import cn.countryplay.utils.QiniuUtil;
import cn.countryplay.utils.RespStateUtil;
import com.alibaba.fastjson.JSONObject;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
/**
 * Author:甲粒子
 * Date: 2022/4/1
 * Description：
 */
@RestController
@Api(value = "CityInfo")
public class CityController {
    public static final int PAGE_SIZE = 10;
    @Autowired
    private ICityService cityService;


    @ApiOperation(value = "分页获取城市信息", notes = "分页获取城市信息")
    @ResponseBody
    @GetMapping(value = "/api/cityInfo/{curPage}",produces = "application/json;charset=UTF-8")
    @Authentication
    public String getCityInfo(@PathVariable long curPage){
        try{
            PageObject<City> page = new PageObject<>();
            page.setCurrentpage(curPage);
            page.setTotalcount(cityService.countCities());
            page.setPagesize(PAGE_SIZE);
            page.setObjects(cityService.getCities(curPage*PAGE_SIZE, PAGE_SIZE));
            return JSONObject.toJSONString(page);
        }catch (Exception e){
            e.printStackTrace();
            return RespStateUtil.state0(new HashMap<>());
        }
    }


    @ApiOperation(value = "获取top25城市信息", notes = "获取top25城市信息")
    @ResponseBody
    @GetMapping(value = "/api/top25cityInfo",produces = "application/json;charset=UTF-8")
    @Authentication
    public String getTop25CityInfo(){
        try{
            return JSONObject.toJSONString(cityService.getTop25Cities());
        }catch (Exception e){
            return RespStateUtil.state0(new HashMap<>());
        }
    }

    @ApiOperation(value = "获取hot城市信息", notes = "获取hot城市信息")
    @ResponseBody
    @GetMapping(value = "/api/hotCityInfo/{curPage}",produces = "application/json;charset=UTF-8")
    @Authentication
    public String getHotCityInfo(@PathVariable long curPage){
        try{
            PageObject<City> page = new PageObject<>();
            page.setCurrentpage(curPage);
            page.setTotalcount(cityService.countCities());
            page.setPagesize(PAGE_SIZE);
            page.setObjects(cityService.getHotCities(curPage*PAGE_SIZE, PAGE_SIZE));
            return JSONObject.toJSONString(page);
        }catch (Exception e){
            return RespStateUtil.state0(new HashMap<>());
        }
    }


    @ApiOperation(value = "添加城市信息", notes = "添加城市信息")
    @ResponseBody
    @PostMapping("/api/addCity")
    @Authentication
    public String addCityInfo(@RequestBody City city,@RequestBody int nums, HttpServletRequest request){
        try{
            if(city != null){
                //上传文件
                MultipartHttpServletRequest req=(MultipartHttpServletRequest)request;
                List<MultipartFile> files = req.getFiles("imgs");
                Map<String, Object> qiniu = QiniuUtil.qiniu(files);
                if((int)qiniu.get("state")==0 || (int)qiniu.get("nums")!=nums){
                    return  RespStateUtil.state0(new HashMap<>());
                }
                String imgs = (String)qiniu.get("imgs");
                city.setBgImages(imgs);
                if (!RespStateUtil.isSuccessfully(cityService.insertCity(city))) {
                    return  RespStateUtil.state0(new HashMap<>());
                }
                return  RespStateUtil.state1(new HashMap<>());
            }
            return RespStateUtil.state0(new HashMap<>());
        }catch (Exception e){
            e.printStackTrace();
            return RespStateUtil.state0(new HashMap<>());
        }
    }

    @ApiOperation(value = "修改城市背景图", notes = "修改城市背景图【用于后台】")
    @ResponseBody
    @PostMapping("/api/updateCityBg")
    @Authentication
    public String updateCityBg(@RequestBody String images,@RequestBody int cityId){
        if(images != null && cityId != 0){
            if (RespStateUtil.isSuccessfully( cityService.setBgImages(cityId,images))) {
                return RespStateUtil.state1(new HashMap<>());
            }
            return RespStateUtil.state0(new HashMap<>());
        }
        return RespStateUtil.state0(new HashMap<>());
    }


}
