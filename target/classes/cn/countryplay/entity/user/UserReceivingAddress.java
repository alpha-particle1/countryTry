package cn.countryplay.entity.user;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.io.Serializable;
/**
 * Author:甲粒子
 * Date: 2022/3/26
 * Description：
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserReceivingAddress implements Serializable {
    private int id;
    private Integer userId;
    private String phone;
    private String name;
    private String stamp;
    private String receivingAddress;
}
