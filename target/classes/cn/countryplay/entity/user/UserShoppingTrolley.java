package cn.countryplay.entity.user;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import java.io.Serializable;
/**
 * Author:甲粒子
 * Date: 2022/3/26
 * Description：购物车 存redis
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "userShoppingTrolley")
public class UserShoppingTrolley implements Serializable {
    @Id
    private String id;
    private String commodityId;//商品号
    private Integer userId;//用户id
    private String cTime;//加入购物车时间
}
