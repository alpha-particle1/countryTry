package cn.countryplay.entity.user;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.io.Serializable;
/**
 * Author:甲粒子
 * Date: 2022/3/26
 * Description：
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class User implements Serializable {
    private int id;//自增id
    private Integer userId;//id
    private String openId;//openid
    private String nickName;//昵称
    private int gender;//性别
    private String avatar;//头像
    private String phone;//电话
    private String email;//邮箱
    /**
     * 账号状态
     * Description： -1封号
     *               0解封中
     *               1正常
     */
    private int status;//状态
    private String cTime;//账号创建时间
    private String rTime;//最新登录时间
    private int version;//版本号
}
