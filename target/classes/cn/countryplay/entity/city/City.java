package cn.countryplay.entity.city;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.io.Serializable;

/**
 * Author:甲粒子
 * Date: 2022/3/26
 * Description：城市
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class City implements Serializable {
    private int id;//城市id
    private String name;//城市名
    private String province;//省份
    private long likeNums;//喜欢的人数
    private String bgImages;//背景图片
}
