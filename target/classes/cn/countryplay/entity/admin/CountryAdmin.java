package cn.countryplay.entity.admin;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * Author:甲粒子
 * Date: 2022/3/24
 * Description：
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CountryAdmin implements Serializable {
    private int id;
    private String username;
    private String userpwd;
    private String nickname;
}