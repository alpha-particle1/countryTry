package cn.countryplay.entity.country;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * Author:甲粒子
 * Date: 2022/3/26
 * Description：乡村
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Country implements Serializable {
    private int id;//乡村id
    private int cityId;//城市id
    private String name;//名称
    private String about;//简介
    private double score;//评分(星级)
    private int likeNums;//喜欢的人数
    private String images;//图片
}
