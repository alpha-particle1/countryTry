package cn.countryplay.entity.country;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;

/**
 * Author:甲粒子
 * Date: 2022/3/26
 * Description：review
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "countryComment")
public class CountryComment implements Serializable {
    @Id
    private String id; //评论id
    private int countryId;//乡村id
    private Integer userId;//评论人id
    private Integer u2Id;//上级回复
    private String content;//评论内容
    private String ctime;//评论时间
}
