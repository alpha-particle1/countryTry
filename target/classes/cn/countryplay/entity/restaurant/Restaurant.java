package cn.countryplay.entity.restaurant;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * Author:甲粒子
 * Date: 2022/3/26
 * Description：
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Restaurant implements Serializable {
    private int id;//餐馆id
    private int countryId;//乡村id
    private Integer userId;//所属人id
    private String name;//餐馆名
    private String about;//简介
    private String imgs;//图片
    private String score;//评分
    private String address;//地址
    private String phone;//电话
}
