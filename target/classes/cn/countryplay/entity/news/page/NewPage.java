package cn.countryplay.entity.news.page;

import cn.countryplay.entity.news.News;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * Author:甲粒子
 * Date: 2021/12/17
 * Description：西华新闻
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class NewPage implements Serializable {
    private long totalpage;
    private long currentpage;
    private long totalcount;
    private long pagesize;
    private List<News> news;
    public void setPagesize(int psize){
        this.pagesize=psize;
        totalpage=totalcount%pagesize!=0?totalcount/pagesize+1:totalcount/pagesize;
    }
}
