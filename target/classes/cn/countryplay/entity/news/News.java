package cn.countryplay.entity.news;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;

/**
 * Author:甲粒子
 * Date: 2022/3/21
 * Description：中国旅游网新闻
 */
@Document(collection = "countryNews")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class News implements Serializable {
    @Id
    private String id;
    private String news_title;//新闻标题
    private String news_ctime;//发布时间
    private String news_author;//发布作者
    private String news_article;//新闻正文
    private String news_imgs;//图片地址 以 "|"分割

    @Override
    public String toString() {
        return "New{" +
                "new_title='" + news_title + '\'' +
                ", new_ctime='" + news_ctime + '\'' +
                ", new_author='" + news_author + '\'' +
                ", new_article='" + news_article + '\'' +
                ", new_imgs='" + news_imgs + '\'' +
                '}';
    }
}