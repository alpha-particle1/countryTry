package cn.countryplay.entity.annotation;

import java.lang.annotation.*;

@Documented
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Authentication {
    int[] role() default {1,2,3,1015,1016};
}
