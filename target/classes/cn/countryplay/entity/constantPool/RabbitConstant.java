package cn.countryplay.entity.constantPool;

/**
 * Author:甲粒子
 * Date: 2022/4/6
 * Description：RabbitMQ使用的交换器名等
 */
public class RabbitConstant {
    /**
     * 交换机
     */
    public static final String TOPIC_EXCHANGE = "topic.exchange";

    /**
     * 订单消息：交换机
     */
    public static final String TOPIC_ORDER_EXCHANGE = "topic.order.exchange";

    /**
     * 订单消息：死信交换机
     */
    public static final String DEAD_LETTER_EXCHANGE_ORDER = "dead.letter.exchange.order";

    /**
     * 邮件队列
     */
    public static final String EMAIL_QUEUE = "email";

    /**
     * 订单队列
     */
    public static final String ORDER_QUEUE = "order";

    /**
     * 订单死信队列
     */
    public static final String DEAD_LETTER_QUEUE_ORDER = "dead.letter.order";

    /**
     * 短信队列
     */
    public static final String MESSAGE_QUEUE = "message";
    /**
     * 邮件队列路由键
     */
    public static final String EMAIL_ROUTING_KEY = "email.key";
    /**
     * 订单队列路由键
     */
    public static final String ORDER_ROUTING_KEY = "order.key";
    /**
     * 订单队列到死信交换机的路由键
     */
    public static final String ORDER_DEAD_LETTER_EX_ROUTING_KEY = "order_dlx_key";
    /**
     * 订单队列： 死信交换机到死信队列的路由键
     */
    public static final String DEAD_LETTER_EX_TO_Q_ROUTING_KEY = "order.dlx.to.dlq.key";
    /**
     * 短信队列路由键
     */
    public static final String MESSAGE_ROUTING_KEY = "message.key";

}
