package cn.countryplay.entity.commodity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import java.io.Serializable;
/**
 * Author:甲粒子
 * Date: 2022/3/26
 * Description：
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "commodityComment")
public class CommodityComment implements Serializable {
    @Id
    private String id; //评论id
    private int commodityId;//商品id
    private Integer userId;//评论人id
    private Integer u2Id;//上级回复
    private String content;//评论内容
    private String ctime;//评论时间
}
