package cn.countryplay.entity.star;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Author:甲粒子
 * Date: 2022/4/9
 * Description：乡村评分
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "countryStar")
public class CountryStar {
    @Id
    private String id;
    private int countryId;
    private int score;
    private int scoreNums;
}
