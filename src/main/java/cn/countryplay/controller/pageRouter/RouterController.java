package cn.countryplay.controller.pageRouter;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
/**
 * Author:甲粒子
 * Date: 2022/3/24
 * Description：页面路由
 */
@Controller
public class RouterController {

    @ResponseBody
    @GetMapping("/")
    public String root(){
        return "";
    }
    //登录页面路由
    @GetMapping("/login")
    public String login(){
        return "login";
    }

    @ResponseBody
    @PostMapping("/requestLogin")
    public void requestLogin(
            @RequestParam(value = "userName",required = false)String username,
            @RequestParam(value = "userPwd",required = false)String userpwd){
    }

    @PostMapping("/countryplay/main")
    public String main(){
        return "index";
    }


    @GetMapping("/controlpanel")
    public String controlpanel(){
        return "controlpanel";
    }
}
