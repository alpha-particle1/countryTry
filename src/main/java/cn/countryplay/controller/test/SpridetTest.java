package cn.countryplay.controller.test;

import cn.countryplay.service.sprider.ISpriderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Author:甲粒子
 * Date: 2022/3/21
 * Description：
 */
@RestController
public class SpridetTest {

    @Autowired
    ISpriderService spriderService;

    @ResponseBody
    @RequestMapping("/sprider")
    public void sprider(){
        spriderService.takeNewfromCTD("");
    }
}
