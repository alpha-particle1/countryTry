package cn.countryplay.controller.test;

import cn.countryplay.entity.annotation.Authentication;
import cn.countryplay.entity.city.City;
import cn.countryplay.entity.constantPool.RabbitConstant;
import cn.countryplay.entity.user.User;
import cn.countryplay.entity.user.UserAccount;
import cn.countryplay.entity.user.UserOrder;
import cn.countryplay.entity.user.UserReceivingAddress;
import cn.countryplay.service.city.ICityService;
import cn.countryplay.service.user.IUserService;
import cn.countryplay.utils.JWTUtil;
import cn.countryplay.utils.RespStateUtil;
import cn.countryplay.utils.TimeUtil;
import cn.countryplay.utils.redis.RedisUtil;
import com.alibaba.fastjson.JSONObject;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Author:甲粒子
 * Date: 2022/3/27
 * Description：测试
 */
@RestController
@Api("测试用")
public class ServiceTest {
    @Autowired
    private ICityService cityService;
    @Autowired
    private IUserService userService;
    @Autowired
    private RedisUtil redis;

    //@ResponseBody
    //@RequestMapping(value = "/t1",produces = "application/json;charset=UTF-8")
    public String getCities(){
        List<City> cities = cityService.getCities(0, 2);
        String s1 = JSONObject.toJSONString(cities);
        List<City> top25Cities = cityService.getTop25Cities();
        String s2 = JSONObject.toJSONString(top25Cities);
        String s3 = cityService.countCities()+"";
        List<City> hotCities = cityService.getHotCities(0, 2);
        String s4 = JSONObject.toJSONString(hotCities);
        String s5 = cityService.incLikeNums(1)+"";
        String s6 = cityService.setCityName(1, "成都")+"";
        String s7 = cityService.setCityProvince(1, "四川")+"";
        City city = new City();
        city.setName("隧林市");
        city.setProvince("四川省");
        city.setBgImages("imgs");
        cityService.insertCity(city);
        return s1+"\n"+s2+"\n"+s3+"\n"+s4+"\n"+s5+"\n"+s6+"\n"+s7+"\n";
    }


//    @ResponseBody
//    @RequestMapping(value = "/t2",produces = "application/json;charset=UTF-8")
    public String users(){
        List<UserOrder> userOrders = userService.getUserOrders(123, -1, 0, 3);
        UserReceivingAddress userReceivingAddress = userService.getUserReceivingAddress(123);
        UserAccount userAccount = new UserAccount();
        userAccount.setUserId(11223344);
        userAccount.setCTime("2022");
        userAccount.setMoney(100);
        userAccount.setPwd("qwerty");
        //userService.newUserAccount(userAccount);
        User user = new User();
        user.setUserId(123);
        user.setAvatar("https://www");
        user.setEmail("22@qq.com");
        user.setCTime(TimeUtil.getNowTime());
        user.setNickName("小翟");
        user.setOpenId("jadjkajnsd");
        user.setPhone("15902824256");
        user.setGender(1);
        user.setStatus(1);
        user.setRTime(TimeUtil.getNowTime());
        System.out.println(JSONObject.toJSONString(user));
        return JSONObject.toJSONString(userReceivingAddress);
    }

    @ApiOperation(value = "获取新的token",notes = "获取新的token")
    @ResponseBody
    @RequestMapping(value = "/api/getToken",produces = "application/json;charset=UTF-8")
    public String getTestToken(){
        Map<String,String> map = new HashMap<>();

        map.put("userId","123");
        String token = JWTUtil.createToken(map);
        //System.out.println(token);
        //DecodedJWT tokenPayload = JWTUtil.getTokenPayload(token);
        // System.out.println(tokenPayload.getClaim("role").asString());
        return token;
    }


    @ResponseBody
    @RequestMapping(value = "/t4",produces = "application/json;charset=UTF-8")
    @Authentication
    public String t4(){
        return "123";
    }


    @ResponseBody
    @RequestMapping(value = "/t5/{id}",produces = "application/json;charset=UTF-8")
    public String t5(@PathVariable long id){
        try {
            Boolean userIds1 = redis.getBit("userIds", id);
            //返回的是设置之前的状态
            //value是要设置的值
            Boolean userIds2 = redis.setBit("userIds", id, true);
            System.out.println(userIds1 + " "+userIds2);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return "123";
    }


    @Autowired
    private RabbitTemplate rabbit;

    @ResponseBody
    @RequestMapping(value = "/t6/",produces = "application/json;charset=UTF-8")
    public String producer(){
       /* try {
            Map map = new HashMap();
            map.put("邮件","123");
            rabbit.convertAndSend(RabbitConstant.TOPIC_EXCHANGE, RabbitConstant.EMAIL_ROUTING_KEY, map);
        } catch (Exception e) {
            e.printStackTrace();
        }*/


        return "ok";
    }

    @ResponseBody
    @RequestMapping(value = "/t7/{str}",produces = "application/json;charset=UTF-8")
    public String _T_(@PathVariable String str){

        return str;
    }


}
