package cn.countryplay.controller.restController;

import cn.countryplay.entity.annotation.Authentication;
import cn.countryplay.entity.constantPool.RabbitConstant;
import cn.countryplay.entity.user.UserOrder;
import cn.countryplay.service.commodity.ICommodityService;
import cn.countryplay.service.user.IUserService;
import cn.countryplay.utils.RespStateUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;

/**
 * Author:甲粒子
 * Date: 2022/4/7
 * Description：
 */
@Api(value = "订单业务接口")
@RestController
public class OrderController {
    @Autowired
    private IUserService userService;
    @Autowired
    private RabbitTemplate rabbit;
    @Autowired
    private ICommodityService commodityService;

    @ApiOperation(value = "提交订单", notes = "提交订单")
    @ResponseBody
    @PostMapping(value = "/api/order/submit", produces = "application/json;charset=UTF-8")
    @Authentication
    public String submitOrder(@RequestBody UserOrder userOrder){
        try{
            if(userOrder != null){
                //设置订单号
               if( RespStateUtil.isSuccessfully(userService.newUserOrder(userOrder))){
                   //将消息发送到order队列
                   rabbit.convertAndSend(RabbitConstant.TOPIC_ORDER_EXCHANGE,RabbitConstant.ORDER_ROUTING_KEY,userOrder);
                   //rabbit.convertAndSend(RabbitConstant.DEAD_LETTER_EXCHANGE_ORDER,RabbitConstant.DEAD_LETTER_EX_TO_Q_ROUTING_KEY,userOrder);
                   //减少商品数量
                   commodityService.checkNumsAndMinus(userOrder.getCommodityId(),userOrder.getNums());
                   return RespStateUtil.state1(new HashMap<>());
                }
            }
            return RespStateUtil.state0(new HashMap<>());
        }catch (Exception e){
            e.printStackTrace();
            return RespStateUtil.state0(new HashMap<>());
        }
    }

}
