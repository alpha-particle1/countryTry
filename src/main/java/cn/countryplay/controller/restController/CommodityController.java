package cn.countryplay.controller.restController;

import cn.countryplay.entity.annotation.Authentication;
import cn.countryplay.entity.commodity.Commodity;
import cn.countryplay.entity.user.UserOrder;
import cn.countryplay.service.commodity.ICommodityService;
import cn.countryplay.service.user.IUserService;
import cn.countryplay.utils.RespStateUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;

/**
 * Author:甲粒子
 * Date: 2022/4/7
 * Description：商品
 */
@Api(value = "商品")
@RestController
public class CommodityController {
    @Autowired
    private ICommodityService commodityService;

    @ApiOperation(value = "上架商品", notes = "上架商品")
    @ResponseBody
    @PostMapping(value = "/api/commodity/add", produces = "application/json;charset=UTF-8")
    @Authentication
    public String addCommodity(@RequestBody Commodity commodity){
        try{
            commodityService.addCommodity(commodity);
            return RespStateUtil.state1(new HashMap<>());
        }catch (Exception e){
            return RespStateUtil.state0(new HashMap<>());
        }
    }

}
