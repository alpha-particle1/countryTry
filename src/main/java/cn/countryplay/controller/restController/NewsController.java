package cn.countryplay.controller.restController;

import cn.countryplay.entity.news.News;
import cn.countryplay.entity.news.page.NewPage;
import cn.countryplay.service.news.ICountryNewsService;
import com.alibaba.fastjson.JSONObject;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Author:甲粒子
 * Date: 2022/3/22
 * Description：
 */
@Api(value = "NewsInfo")
@RestController
public class NewsController {

    @Autowired
    private ICountryNewsService countryNewsService;

    @ApiOperation(value = "分页获取中国旅游网新闻", notes = "分页获取中国旅游网新闻")
    @ResponseBody
    @RequestMapping(value = "/News/{curPage}",produces = "application/json;charset=UTF-8")
    public String getXhuNews(@PathVariable long curPage){
        if(curPage==0){
            curPage=1;
        }
        Map<String,Object> map = new HashMap<>();
        NewPage newPage = new NewPage();
        newPage.setCurrentpage(curPage);
        long l = countryNewsService.countNews();
        newPage.setTotalcount(l);
        newPage.setPagesize(5);
        List<News> news = countryNewsService.selectNewsPage(curPage, 8);
        map.put("state",1);
        map.put("data",news);
        return JSONObject.toJSONString(map);
    }

}
