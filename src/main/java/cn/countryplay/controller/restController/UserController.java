package cn.countryplay.controller.restController;
import cn.countryplay.aop.VerifyPermissions;
import cn.countryplay.entity.annotation.Authentication;
import cn.countryplay.entity.constantPool.RabbitConstant;
import cn.countryplay.entity.user.User;
import cn.countryplay.entity.user.UserAccount;
import cn.countryplay.entity.user.UserRoles;
import cn.countryplay.service.user.IUserService;
import cn.countryplay.utils.IdUtil;
import cn.countryplay.utils.JWTUtil;
import cn.countryplay.utils.RespStateUtil;
import cn.countryplay.utils.TimeUtil;
import cn.countryplay.utils.redis.RedisUtil;
import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
/**
 * Author:甲粒子
 * Date: 2022/3/31
 * Description：用户服务请求
 */
@RestController
@Api(value = "USERINFO")
public class UserController {
    @Autowired
    private IUserService userService;
    @Autowired
    private RedisUtil redis;
    @Autowired
    private RabbitTemplate rabbit;

    /**
     * 更新用户信息: phone
     * @param data 是json字符串
     * @param request 用于获取请求头中的token
     * ******************* ****************
     * 说明：
     * 1、第一次登录 token一定为空，token为空不一定为第一次登录
     *
     * 2、当token为null时要从微信服务器拿到openId在自己服务器上查询来判断是否第一次登录
     *
     * 3、第一次登录需要解密加密信息，后面登录不需要
     *
     * 4、登录成功返回token，token中包含userId;
     *
     * 5、微信小程序前端调用wx.login会导致服务器后台的sessionKey变化
     *                即：如果此时拿服务器上的sessionKey解密前端加密的数据会出问题的
     * @return token
     */
    @ApiOperation(value = "微信登录", notes = "微信登录")
    @ResponseBody
    @PostMapping("/api/user/wxLogin")
    public String wxLogin(@RequestBody String data, HttpServletRequest request) throws JsonProcessingException {
        String token = request.getHeader("token");
        //携带code iv encryptedData
        ObjectNode node = null;
        //微信服务器返回的数据有sessionKey和openId
        ObjectNode dataNode = null;

        try{
            node = new ObjectMapper().readValue(data, ObjectNode.class);
            String _data_ = userService.requestWX(data, node, new HashMap<>());
            dataNode = userService.resolveData(_data_);

            String openId = dataNode.get("openid").toString();

            if(token == null || "".equals(token)){
                //不是第一次登录
                if (RespStateUtil.isSuccessfully( userService.selectUser(openId))) {
                    //更新登录时间并返回新token
                    Integer userId = userService.selectUserId(openId);
                    if (RespStateUtil.isSuccessfully(userService.setRTime(userId,TimeUtil.getNowTime()))){
                        String newToken = JWTUtil.doCreateToken(new HashMap<>(), userId);
                        Map<String,Object> returnMap = new HashMap<>();
                        returnMap.put("state",1);
                        returnMap.put("token",newToken);
                        return JSONObject.toJSONString(returnMap);
                    }
                    return RespStateUtil.state0(new HashMap<>());
                }

                //是第一次登录

                //node指向解密后的数据节点
                //包含昵称等信息
                node = userService.resolveEncryptedData(node,dataNode);
                //新建用户
                User user = new User();
                user.setNickName(node.get("nickName").toString());
                user.setGender(Integer.parseInt(String.valueOf(node.get("gender"))));
                user.setAvatar(node.get("avatarUrl").toString());
                user.setCTime(TimeUtil.getNowTime());
                user.setRTime(TimeUtil.getNowTime());
                user.setOpenId(openId);
                //生成userId
                Integer userId = 0;
                do {
                    userId = IdUtil.generateUserId();
                }while (redis.getBit("userIds",userId));//自旋
                redis.setBit("userIds",userId,true);
                user.setUserId(userId);

                //新用户及账户创建成功
                if (RespStateUtil.isSuccessfully(userService.newUserAndOpenAccount(user, new UserAccount()))) {
                    //返回token
                    String userToken = JWTUtil.doCreateToken(new HashMap<>(), userId);
                    Map<String,Object> returnMap = new HashMap<>();
                    returnMap.put("state",1);
                    returnMap.put("token",userToken);
                    return JSONObject.toJSONString(returnMap);
                }
                return RespStateUtil.state0(new HashMap<>());
            }
            //不是第一次登录
            else {
                //更新登录时间并返回新token
                Integer userId = userService.selectUserId(openId);
                if (RespStateUtil.isSuccessfully(userService.setRTime(userId,TimeUtil.getNowTime()))){
                    String newToken = JWTUtil.doCreateToken(new HashMap<>(), userId);
                    Map<String,Object> returnMap = new HashMap<>();
                    returnMap.put("state",1);
                    returnMap.put("token",newToken);
                    return JSONObject.toJSONString(returnMap);
                }
                return RespStateUtil.state0(new HashMap<>());
            }

        }catch (Exception e){
            e.printStackTrace();
            return RespStateUtil.stateError(new HashMap<>());
        }

    }

    /**
     * 更新用户信息
     * @param user
     * @return
     */
    @ApiOperation(value = "更新用户信息", notes = "更新用户信息")
    @ResponseBody
    @PostMapping("/api/user/updateUserInfo")
    @Authentication
    public String updateUserInfo(@RequestBody User user){
        try{
            if(user != null){
                user.setRTime(TimeUtil.getNowTime());
                if(!RespStateUtil.isSuccessfully(userService.updateUserInfo(user))){
                    return RespStateUtil.state0(new HashMap<>());
                }
                return RespStateUtil.state1(new HashMap<>());
            }
            return RespStateUtil.state0(new HashMap<>());
        }catch (Exception e){
            e.printStackTrace();
            return RespStateUtil.state0(new HashMap<>());
        }
    }

    @ApiOperation(value = "新增用户信息", notes = "新增用户信息")
    @ResponseBody
    @PostMapping("/api/user/addUser")
    @Authentication
    public String addUser(@RequestBody User user){
        try{
            if(user != null){
                user.setRTime(TimeUtil.getNowTime());
                if(!RespStateUtil.isSuccessfully(userService.newUser(user))){
                    return RespStateUtil.state0(new HashMap<>());
                }
                return RespStateUtil.state1(new HashMap<>());
            }
            return RespStateUtil.state0(new HashMap<>());
        }catch (Exception e){
            e.printStackTrace();
            return RespStateUtil.state0(new HashMap<>());
        }
    }


    @ApiOperation(value = "获取用户权限列表", notes = "获取用户权限列表")
    @ResponseBody
    @GetMapping("/api/user/getUserRoles")
    @Authentication
    public String getUserRoles(@RequestBody Integer userId){
        Map<String,Integer> map = new HashMap<>();
        try{
            if(userId != 0){
                List<UserRoles> userRoles = userService.getUserRoles(userId);
                return JSONObject.toJSONString(userRoles);
            }
            return RespStateUtil.state0(map);
        }catch (Exception e){
            e.printStackTrace();
            return RespStateUtil.state0(map);
        }
    }

    @ApiOperation(value = "获取用户改密(支付密码)验证码", notes = "获取用户改密(支付密码)验证码")
    @ResponseBody
    @GetMapping("/api/user/requestCode")
    @Authentication
    public String producer(HttpServletRequest request){

        try {
            Map map = new HashMap();
            //从token中拿到userId
            Integer userId = VerifyPermissions.getUserIdFromToken(request.getHeader("token"));
            //查询user的Email
            String userEmail = userService.getUserEmail(userId);
            if (userEmail.equals("") || userEmail == null){
                return RespStateUtil.state0(new HashMap<>());
            }
            map.put("userId",userId);
            map.put("email",userEmail);
            rabbit.convertAndSend(RabbitConstant.TOPIC_EXCHANGE, RabbitConstant.EMAIL_ROUTING_KEY, map);
            return RespStateUtil.state1(new HashMap<>());
        } catch (Exception e) {
            e.printStackTrace();
            return RespStateUtil.state0(new HashMap<>());
        }
    }
}
