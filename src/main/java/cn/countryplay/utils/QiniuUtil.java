package cn.countryplay.utils;
import com.alibaba.fastjson.JSONObject;
import com.qiniu.http.Response;
import com.qiniu.storage.Configuration;
import com.qiniu.storage.Region;
import com.qiniu.storage.UploadManager;
import com.qiniu.storage.model.DefaultPutRet;
import com.qiniu.util.Auth;
import org.springframework.web.multipart.MultipartFile;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
public class QiniuUtil {

    private static final String AK="TlJlrKPYwcp0HxIQaidv-9PK9ErCqeY0m5Mi6Fpi";
    private static final String SK="vLHZXID-kzJitYhU5djAweteYIblzgMmBosRwvps";

    private static final String URL_PREFIX="https://img.wxd2114.cn/";
    public static  Map<String,Object> qiniu(List<MultipartFile> files){
        Configuration configuration=new Configuration(Region.region2());
        UploadManager uploadManager=new UploadManager(configuration);
        String key=null;//默认不指定key的情况下，以文件内容的hash值作为文件名

        String imgs="";
        int count=0;
        Map<String,Object> map = new HashMap<>();
        try{
            Auth auth=Auth.create(AK,SK);
            String upToken=auth.uploadToken("countryplay");
            Response response;
            for (MultipartFile file : files) {
                response=uploadManager.put(file.getBytes(),key,upToken);
                DefaultPutRet putRet = JSONObject.parseObject(response.bodyString(), DefaultPutRet.class);
                imgs +=(URL_PREFIX+putRet.key+"|");//注意最后多一个"|"
                ++count;
            }
            map.put("state",1);
            map.put("nums",count);
            map.put("imgs",imgs);
            return  map;

        }catch (Exception e){
            e.printStackTrace();
            map.put("state",0);
            map.put("nums",count);
            map.put("imgs",imgs);
            return map;
        }
    }
}
