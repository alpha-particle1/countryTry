package cn.countryplay.entity.user;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Author:甲粒子
 * Date: 2022/3/26
 * Description：
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserAccount {
    private int id;//自增id
    private Integer userId;//id
    private long money;
    /**
     * 账户状态
     * Description： -1冻结
     *               0解冻中
     *               1正常
     */
    private int status;
    private String cTime;//创建时间
    private String rTime;//最近使用时间
    private String pwd;//支付密码
}
