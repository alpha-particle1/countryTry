package cn.countryplay.entity.user;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.io.Serializable;
/**
 * Author:甲粒子
 * Date: 2022/3/26
 * Description：订单
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserOrder implements Serializable {
    private int id;
    private String orderId;//订单id
    private Integer userId;//用户id
    private Integer saleId;//卖家id
    private String commodityId;//商品号
    private int nums;//购买的数量
    /**
     * 订单状态
     * Description：-2已过期
     *              -1待支付
     *               0取消支付
     *               1已支付
     *               2退款中
     *               3已退款
     *               4已发货
     *               5已签收
     */
    private int status;
    private String cTime;
    private long stopTime;//截止时间：到该时间后订单失效

}
