package cn.countryplay.entity.user;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * Author:甲粒子
 * Date: 2022/3/26
 * Description：
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserRoles implements Serializable {
    private int id;//自增id
    private Integer userId;//用户id
    private int roleId;//权限id
}
