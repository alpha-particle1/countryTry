package cn.countryplay.entity.post;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import java.io.Serializable;
/**
 * Author:甲粒子
 * Date: 2022/3/26
 * Description：
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "post")
public class Post implements Serializable {
    //id
    @Id
    private String id; //帖子id
    private int countryId;//乡村id;
    //userid（Inteher）
    private Integer userid;//发帖人id
    //content （内容）
    private String content;
    //ctime 创建时间
    private String ctime;
    //status  状态（1为可显示，0为异常不可显示（查库时跳过此状态的数据））
    private int status;
    //imgPath  图片的路径(涉及文件上传)：
    private String imgPath;
    //上传到 jar包根目录img路径下  img/userid/时间戳(年月)/帖子id/  ..... .png
    //返回时带上前缀 https://www.xhubbq.cn/+img/userid/时间戳(年月)/帖子id/  ..... .png
    //多图片 路径以 "|"分割
    //点赞数
    private Integer likeNum;
    //点击量
    private long touchNum;
}
