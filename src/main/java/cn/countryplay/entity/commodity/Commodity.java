package cn.countryplay.entity.commodity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;

/**
 * Author:甲粒子
 * Date: 2022/3/26
 * Description：commodity
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "commodity")
public class Commodity implements Serializable {
    @Id
    private String id;//商品号
    private Integer userId;//上传者id
    private String upTime;//上架时间
    private String description;//描述
    private String title;//标题
    private String images;//图
    private Double price0;//原价
    private Double price;//促销价
    private Double freight;//运费
    private long nums;//剩余的数量
    private int sale;//销量
    //1：农产品
    //2：手工制品
    private int type;//类型
}
