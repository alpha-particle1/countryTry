package cn.countryplay.entity.hotel;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * Author:甲粒子
 * Date: 2022/3/26
 * Description：酒店
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Hotel implements Serializable {
    private int id;//酒店id
    private int countryId;//乡村id
    private Integer userId;//所属人id
    private String name;//酒店名
    private String about;//简介
    private String imgs;//图片
    private String score;//评分
    private String address;//地址
    private String phone;//电话
}
