package cn.countryplay.service.admin;
import org.springframework.security.core.userdetails.UserDetailsService;
/**
 * Author:甲粒子
 * Date: 2022/3/24
 * Description：
 */
public interface IAdminService extends UserDetailsService {

}
