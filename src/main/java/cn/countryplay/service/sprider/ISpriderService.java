package cn.countryplay.service.sprider;
import cn.countryplay.entity.news.News;

import java.util.List;

/**
 * Author:甲粒子
 * Date: 2022/3/21
 * Description：中国旅游网新闻爬取
 */
public interface ISpriderService {
    String CHINA_TRAVEL_DOMAIN = "http://www.cntour.cn/";

    //中国旅游网新闻爬取
    List<News> takeNewfromCTD(String url);
}
