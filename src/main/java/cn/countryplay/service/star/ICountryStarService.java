package cn.countryplay.service.star;

import cn.countryplay.entity.star.CountryStar;

/**
 * Author:甲粒子
 * Date: 2022/4/9
 * Description：
 */
public interface ICountryStarService {
    /**
     * 新增乡村评分
     * @param countryStar
     * @return
     */
    int starCountry(CountryStar countryStar);

    /**
     * 乡村评分
     * @param countryId 乡村id
     * @param score 评分
     * @return
     */
    int starCountry(int countryId,int score);

    /**
     * 乡村评分平均分
     * @param countryId 乡村id
     * @return
     */
    float getCountryStar(int countryId);
}
