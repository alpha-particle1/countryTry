package cn.countryplay.service.commodity.impl;

import cn.countryplay.entity.commodity.Commodity;
import cn.countryplay.entity.user.UserShoppingTrolley;
import cn.countryplay.mapper.commodity.ICommodityDao;
import cn.countryplay.service.commodity.ICommodityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
/**
 * Author:甲粒子
 * Date: 2022/4/7
 * Description：
 */
@Service
public class CommodityServiceImpl implements ICommodityService {
    @Autowired
    private ICommodityDao commodityDao;
    @Override
    public int addCommodity(Commodity commodity) {
        return commodityDao.addCommodity(commodity);
    }

    @Override
    public int offShelves(String cid) {
        return commodityDao.offShelves(cid);
    }

    @Override
    public int addCommodityNums(String commodityId, int nums) {
        return commodityDao.addCommodityNums(commodityId,nums);
    }

    @Override
    public int minusCommodityNums(String commodityId, int nums) {
        return commodityDao.minusCommodityNums(commodityId,nums);
    }

    @Override
    public long getCommodityNums(String commodityId) {
        return commodityDao.getCommodityNums(commodityId);
    }

    @Override
    public List<Commodity> getCommodityPage(long curPage, int pageSize) {
        return commodityDao.getCommodityPage(curPage,pageSize);
    }

    @Override
    public List<Commodity> getCommodityPage(String key, int type, int srt, long curPage, int pageSize) {
        return commodityDao.getCommodityPage(key,type,srt,curPage,pageSize);
    }

    @Override
    public long countCommodity() {
        return commodityDao.countCommodity();
    }

    @Override
    public long countCommodity(String key, int type) {
        return commodityDao.countCommodity(key,type);
    }

    @Override
    public int joinTheShoppingCart(UserShoppingTrolley shopping) {
        return commodityDao.joinTheShoppingCart(shopping);
    }

    @Override
    public int removeTheShoppingCart(Integer userId, String commodityId) {
        return commodityDao.removeTheShoppingCart(userId, commodityId);
    }

    @Override
    public List<Commodity> getTheShoppingCartByPage(Integer userId, long curPage, int pageSize) {
        return commodityDao.getTheShoppingCartByPage(userId, curPage, pageSize);
    }

    @Override
    public long countTheShoppingCart(Integer userId) {
        return commodityDao.countTheShoppingCart(userId);
    }

    @Override
    public void checkNumsAndMinus(String commodityId, int nums) {
        if(getCommodityNums(commodityId)>=nums){
            minusCommodityNums(commodityId,nums);
        }
    }
}
