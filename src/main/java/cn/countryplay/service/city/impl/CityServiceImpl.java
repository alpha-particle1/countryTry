package cn.countryplay.service.city.impl;

import cn.countryplay.entity.city.City;
import cn.countryplay.mapper.city.CityMapper;
import cn.countryplay.service.city.ICityService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Author:甲粒子
 * Date: 2022/3/27
 * Description：
 */
@Service
public class CityServiceImpl implements ICityService {
    @Resource
    private CityMapper cityMapper;

    @Override
    public List<City> getCities(long curPage, int pageSize) {
        return cityMapper.getCities(curPage,pageSize);
    }

    @Override
    public List<City> getHotCities(long curPage, int pageSize) {
        return cityMapper.getHotCities(curPage,pageSize);
    }

    @Override
    public List<City> getTop25Cities() {
        return cityMapper.getTop25Cities();
    }

    @Override
    public long countCities() {
        return cityMapper.countCities();
    }

    @Override
    public int incLikeNums(int cityId) {
        return cityMapper.incLikeNums(cityId);
    }

    @Override
    public int setBgImages(int cityId, String images) {
        return cityMapper.setBgImages(cityId,images);
    }

    @Override
    public int setCityName(int cityId, String name) {
        return cityMapper.setCityName(cityId,name);
    }

    @Override
    public int setCityProvince(int cityId, String province) {
        return cityMapper.setCityProvince(cityId,province);
    }

    @Override
    public int insertCity(City city) {
        return cityMapper.insertCity(city);
    }
}
