package cn.countryplay.service.mail;

import cn.countryplay.entity.mail.MailVo;
/**
 * Author:甲粒子
 * Date: 2021/11/11
 * Description：邮件服务
 */
public interface MailService {

    /**
     * 发送纯文本邮件
     * @param-toAddr 发送给谁
     * @param-title 标题
     * @param -content 内容
     */
    public void sendTextMail(MailVo vo);

    /**
     * 发送 html 邮件
     * @param -toAddr
     * @param -title
     * @param -content 内容（HTML）
     */
    public void sendHtmlMail(MailVo vo);

    /**
     *  发送待附件的邮件
     * @param -toAddr
     * @param -title
     * @param -content
     * @param -filePath 附件地址
     */
    public void sendAttachmentsMail(MailVo vo);

    /**
     *  发送文本中有静态资源（图片）的邮件
     * @param -toAddr
     * @param -title
     * @param -content
     * @param -rscPath 资源路径
     * @param -rscId 资源id (可能有多个图片)
     */
    public void sendInlineResourceMail(MailVo vo);

}

