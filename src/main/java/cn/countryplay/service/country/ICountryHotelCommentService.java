package cn.countryplay.service.country;

import cn.countryplay.entity.hotel.HotelComment;
import java.util.List;
/**
 * Author:甲粒子
 * Date: 2022/4/6
 * Description：
 */
public interface ICountryHotelCommentService {
    /**
     * 增加:酒店评论
     * @param comment 评论内容
     * @return
     */
    int comment(HotelComment comment);

    /**
     * 删除评论
     * @param id 评论id
     * @return
     */
    int deleteComment(String id);


    /**
     * 获取乡村id为countryId的评论数量
     * @param hotelId 酒店id
     * @return
     */
    long countComments(int hotelId);

    /**
     *分页查询
     * @param hotelId 酒店id
     * @return 对应id的酒店的评论页
     */
    List<HotelComment> getCommentPage(int hotelId, int curPage, int pageSize);
}
