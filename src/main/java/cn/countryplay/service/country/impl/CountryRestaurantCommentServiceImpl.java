package cn.countryplay.service.country.impl;

import cn.countryplay.entity.restaurant.RestaurantComment;
import cn.countryplay.mapper.country.ICountryRestaurantCommentDao;
import cn.countryplay.service.country.ICountryRestaurantCommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
/**
 * Author:甲粒子
 * Date: 2022/4/6
 * Description：
 */
@Service
public class CountryRestaurantCommentServiceImpl implements ICountryRestaurantCommentService {
    @Autowired
    private ICountryRestaurantCommentDao countryRestaurantCommentDao;
    @Override
    public int comment(RestaurantComment comment) {
        return countryRestaurantCommentDao.comment(comment);
    }

    @Override
    public int deleteComment(String id) {
        return countryRestaurantCommentDao.deleteComment(id);
    }

    @Override
    public long countComments(int restId) {
        return countryRestaurantCommentDao.countComments(restId);
    }

    @Override
    public List<RestaurantComment> getCommentPage(int restId, int curPage, int pageSize) {
        return countryRestaurantCommentDao.getCommentPage(restId,curPage,pageSize);
    }
}
