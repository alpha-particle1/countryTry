package cn.countryplay.service.country.impl;
import cn.countryplay.entity.country.CountryComment;
import cn.countryplay.mapper.country.ICountryCommentDao;
import cn.countryplay.service.country.ICountryCommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
/**
 * Author:甲粒子
 * Date: 2022/3/29
 * Description：
 */
@Service
public class CountryCommentServiceImpl implements ICountryCommentService {
    @Autowired
    private ICountryCommentDao countryCommentDao;

    @Override
    public int comment(CountryComment comment) {
        return countryCommentDao.comment(comment);
    }

    @Override
    public int deleteComment(String id) {
        return countryCommentDao.deleteComment(id);
    }

    @Override
    public long countComments(int countryId) {
        return countryCommentDao.countComments(countryId);
    }

    @Override
    public List<CountryComment> getCountryCommentPage(int countryId, int curPage, int pageSize) {
        return countryCommentDao.getCommentPage(countryId,curPage,pageSize);
    }
}
