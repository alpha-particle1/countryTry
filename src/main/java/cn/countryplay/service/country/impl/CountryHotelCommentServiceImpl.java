package cn.countryplay.service.country.impl;

import cn.countryplay.entity.hotel.HotelComment;
import cn.countryplay.mapper.country.ICountryHotelCommentDao;
import cn.countryplay.service.country.ICountryHotelCommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
/**
 * Author:甲粒子
 * Date: 2022/4/6
 * Description：
 */
@Service
public class CountryHotelCommentServiceImpl implements ICountryHotelCommentService{
    @Autowired
    private ICountryHotelCommentDao countryHotelCommentDao;
    @Override
    public int comment(HotelComment comment) {
        return countryHotelCommentDao.comment(comment);
    }

    @Override
    public int deleteComment(String id) {
        return countryHotelCommentDao.deleteComment(id);
    }

    @Override
    public long countComments(int hotelId) {
        return countryHotelCommentDao.countComments(hotelId);
    }

    @Override
    public List<HotelComment> getCommentPage(int hotelId, int curPage, int pageSize) {
        return countryHotelCommentDao.getCommentPage(hotelId,curPage,pageSize);
    }
}
