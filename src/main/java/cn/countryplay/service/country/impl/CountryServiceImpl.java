package cn.countryplay.service.country.impl;
import cn.countryplay.entity.city.City;
import cn.countryplay.entity.country.Country;
import cn.countryplay.mapper.country.CountryMapper;
import cn.countryplay.service.country.ICountryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
/**
 * Author:甲粒子
 * Date: 2022/3/28
 * Description：
 */
@Service
public class CountryServiceImpl implements ICountryService {
    @Autowired
    private CountryMapper countryMapper;

    @Override
    public int insertCountry(Country country) {
        return countryMapper.insertCountry(country);
    }

    @Override
    public List<City> getCountries(long curPage, int pageSize) {
        return countryMapper.getCountries(curPage,pageSize);
    }

    @Override
    public List<City> getHotCountries(long curPage, int pageSize) {
        return countryMapper.getHotCountries(curPage,pageSize);
    }

    @Override
    public List<City> getHotCountriesOfCity(long curPage, int pageSize, int cityId) {
        return countryMapper.getHotCountriesOfCity(curPage,pageSize,cityId);
    }

    @Override
    public List<City> getTop25Countries() {
        return countryMapper.getTop25Countries();
    }

    @Override
    public long countCountries() {
        return countryMapper.countCountries();
    }

    @Override
    public long countCountriesOfCity(int cityId) {
        return countryMapper.countCountriesOfCity(cityId);
    }

    @Override
    public int incLikeNums(int countryId) {
        return countryMapper.incLikeNums(countryId);
    }

    @Override
    public int setBgImages(int countryId, String images) {
        return countryMapper.setBgImages(countryId,images);
    }

    @Override
    public int setCountryName(int countryId, String name) {
        return countryMapper.setCountryName(countryId,name);
    }

    @Override
    public int setCountyOfCity(int cityId, int countryId) {
        return countryMapper.setCountyOfCity(cityId,countryId);
    }

    @Override
    public int setCountryAbout(int countryId, String about) {
        return countryMapper.setCountryAbout(countryId,about);
    }

    @Override
    public int setCountryScore(int countryId, double score) {
        return countryMapper.setCountryScore(countryId,score);
    }
}
