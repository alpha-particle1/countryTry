package cn.countryplay.service.country;

import cn.countryplay.entity.restaurant.RestaurantComment;
import java.util.List;
/**
 * Author:甲粒子
 * Date: 2022/4/6
 * Description：
 */
public interface ICountryRestaurantCommentService {
    /**
     * 增加:餐馆评论
     * @param comment 评论内容
     * @return
     */
    int comment(RestaurantComment comment);

    /**
     * 删除评论
     * @param id 评论id
     * @return
     */
    int deleteComment(String id);


    /**
     * 获取餐馆id为countryId的评论数量
     * @param restId 餐馆id
     * @return
     */
    long countComments(int restId);

    /**
     *分页查询
     * @param restId 餐馆id
     * @return 对应id的餐馆的评论页
     */
    List<RestaurantComment> getCommentPage(int restId, int curPage, int pageSize);
}
