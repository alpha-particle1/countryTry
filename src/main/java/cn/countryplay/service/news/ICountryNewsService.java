package cn.countryplay.service.news;

import cn.countryplay.entity.news.News;

import java.util.List;

/**
 * Author:甲粒子
 * Date: 2022/3/22
 * Description：中国旅游网新闻服务
 */
public interface ICountryNewsService {
    boolean addNewsfromSprider(News news);
    boolean existedNews(String title);

    long countNews();
    List<News> selectNewsPage(long curPage, int pageSize);
}
