package cn.countryplay.mapper.admin;

import cn.countryplay.entity.admin.RoleTable;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Author:甲粒子
 * Date: 2022/3/24
 * Description：
 */
@Mapper
@Repository
public interface RoleMapper {
    //根据用户的id,查询用户的所用权限
    //@Select("select r.*  from role_table as r where r.id in(select roleid from admin_role where userid=#{userid})")
    List<RoleTable> findRoleByAdminId(int userid);
}
