package cn.countryplay.mapper.admin;
import cn.countryplay.entity.admin.CountryAdmin;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;
/**
 * Author:甲粒子
 * Date: 2022/3/24
 * Description：
 */
@Mapper
@Repository
public interface AdminMapper {
    //这个将用于SpringSecurity
    //@Select("select * from country_admin where username=#{name}")
    CountryAdmin loadAdminByAdminName(String name);
}
