package cn.countryplay.mapper.country.impl;

import cn.countryplay.entity.hotel.HotelComment;
import cn.countryplay.mapper.country.ICountryHotelCommentDao;
import com.mongodb.client.result.DeleteResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
/**
 * Author:甲粒子
 * Date: 2022/4/6
 * Description：
 */
@Repository
public class CountryHotelCommentDaoImpl implements ICountryHotelCommentDao {
    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public int comment(HotelComment comment) {
        HotelComment insert = mongoTemplate.insert(comment);
        if(insert==null){
            return 0;
        }
        return 1;
    }

    @Override
    public int deleteComment(String id) {
        Query query=new Query();
        query.addCriteria(Criteria.where("_id").is(id));
        DeleteResult commentDel = mongoTemplate.remove(query, "hotelComment");
        if(commentDel.getDeletedCount()==0){
            return 0;
        }
        return 1;
    }

    @Override
    public long countComments(int hotelId) {
        Query query=new Query();
        query.addCriteria(Criteria.where("hotelId").is(hotelId));
        return mongoTemplate.count(query,HotelComment.class);
    }

    @Override
    public List<HotelComment> getCommentPage(int hotelId, int curPage, int pageSize) {
        Query query=new Query();
        query.addCriteria(Criteria.where("hotelId").is(hotelId));
        return mongoTemplate.find(query.skip((curPage-1)*pageSize).limit(pageSize),HotelComment.class);
    }
}
