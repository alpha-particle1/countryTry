package cn.countryplay.mapper.country.impl;
import cn.countryplay.entity.country.CountryComment;
import cn.countryplay.mapper.country.ICountryCommentDao;
import com.mongodb.client.result.DeleteResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;
import java.util.List;
/**
 * Author:甲粒子
 * Date: 2022/3/29
 * Description：乡村相关的评论是放在mongodb中集合为countryComment中的
 */
@Repository
public class CountryCommentDaoImpl implements ICountryCommentDao {
    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public int comment(CountryComment comment) {
        CountryComment insert = mongoTemplate.insert(comment);
        if(insert==null){
            return 0;
        }
        return 1;
    }

    @Override
    public int deleteComment(String id) {
        Query query=new Query();
        query.addCriteria(Criteria.where("_id").is(id));
        DeleteResult commentDel = mongoTemplate.remove(query, "countryComment");
        if(commentDel.getDeletedCount()==0){
            return 0;
        }
        return 1;
    }

    @Override
    public long countComments(int countryId) {
        Query query=new Query();
        query.addCriteria(Criteria.where("countryId").is(countryId));
        return mongoTemplate.count(query,CountryComment.class);
    }

    @Override
    public List<CountryComment> getCommentPage(int countryId, int curPage, int pageSize) {
        Query query=new Query();
        query.addCriteria(Criteria.where("countryId").is(countryId));
        return mongoTemplate.find(query.skip((curPage-1)*pageSize).limit(pageSize),CountryComment.class);
    }
}
