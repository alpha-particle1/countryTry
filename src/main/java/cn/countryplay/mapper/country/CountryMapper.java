package cn.countryplay.mapper.country;
import cn.countryplay.entity.city.City;
import cn.countryplay.entity.country.Country;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
/**
 * Author:甲粒子
 * Date: 2022/3/28
 * Description：乡村 dao层
 */
@Mapper
public interface CountryMapper {
    /**
     * 添加城市
     * @param country 添加的乡村信息
     * @return
     */
    int insertCountry(Country country);

    /**
     * 分页获取乡村信息
     * @param curPage 当前页
     * @param pageSize 页面大小
     * @return
     */
    List<City> getCountries(long curPage, int pageSize);

    /**
     * 分页获取热搜乡村信息:按喜欢量倒序排序
     * @param curPage 当前页
     * @param pageSize 页面大小
     * @return
     */
    List<City> getHotCountries(long curPage, int pageSize);

    /**
     * 分页获取某市的热门乡村信息:按喜欢量倒序排序
     * @param curPage 当前页
     * @param pageSize 页面大小
     * @param cityId 城市id
     * @return
     */
    List<City> getHotCountriesOfCity(@Param("curPage")long curPage, @Param("pageSize")int pageSize,@Param("cityId")int cityId);

    /**
     * 获取热搜前25的乡村信息
     * @return
     */
    List<City> getTop25Countries();

    /**
     * 获取乡村数量，用于分页
     * @return
     */
    long countCountries();

    /**
     * 获取某市的乡村数量，用于分页
     * @param cityId 城市id
     * @return
     */
    long countCountriesOfCity(int cityId);

    /**
     * 喜欢人数加1
     * @param countryId 乡村id
     * @return
     */
    int incLikeNums(int countryId);

    /**
     * 设置背景图片
     * @param countryId 乡村id
     * @param images 乡村背景图
     * @return
     */
    int setBgImages(int countryId,String images);

    /**
     * 设置乡村名
     * @param countryId 乡村id
     * @param name 乡村名
     * @return
     */
    int setCountryName(int countryId,String name);

    /**
     * 设置所属城市
     * @param cityId 城市id
     * @param countryId 乡村id
     * @return
     */
    int setCountyOfCity(@Param("cityId") int cityId, @Param("countryId")int countryId);

    /**
     * 设置乡村简介
     * @param countryId 乡村id
     * @param about 乡村名
     * @return
     */
    int setCountryAbout(int countryId,String about);

    /**
     * 设置乡村评分
     * @param countryId 乡村id
     * @param score 乡村名
     * @return
     */
    int setCountryScore(int countryId,double score);

}
