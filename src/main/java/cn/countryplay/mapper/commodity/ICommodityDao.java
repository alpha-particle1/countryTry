package cn.countryplay.mapper.commodity;
import cn.countryplay.entity.commodity.Commodity;
import cn.countryplay.entity.user.UserShoppingTrolley;

import java.util.List;
/**
 * Author:甲粒子
 * Date: 2022/4/7
 * Description：
 */
public interface ICommodityDao {

    //===========================================商品===================================================
    /**
     * 添加商品
     * @param commodity 商品
     * @return
     */
    int addCommodity(Commodity commodity);


    /**
     * 下架商品
     * @param cid 商品id
     * @return
     */
    int offShelves(String cid);

    /**
     * 商品数量+nums
     * @param commodityId 商品
     * @param nums 数量
     * @return
     */
    int addCommodityNums(String commodityId,int nums);

    /**
     * 商品数量-nums
     * @param commodityId 商品
     * @param nums 数量
     * @return
     */
    int minusCommodityNums(String commodityId,int nums);

    /**
     * 商品数量剩余数量
     * @param commodityId 商品
     * @return
     */
    long getCommodityNums(String commodityId);

    /**
     * 分页获取商品信息：非模糊查询
     * 默认时间降序排序
     * @param curPage 当前页
     * @param pageSize 页面大小
     * @return
     */
    List<Commodity> getCommodityPage(long curPage, int pageSize);

    /**
     * 分页获取商品信息：模糊查询
     * @param curPage 当前页
     * @param pageSize 页面大小
     * @param key 查询关键字
     * @param type 商品类型
     * @param srt 排序方式
     * @return
     */
    List<Commodity> getCommodityPage(String key,int type,int srt,long curPage, int pageSize);

    /**
     * 分页获取商品信息的数量查询：非模糊查询
     * @return
     */
    long countCommodity();

    /**
     * 分页获取商品信息的数量查询：模糊查询
     * @param key 关键字
     * @param type 商品类型
     * @return
     */
    long countCommodity(String key,int type);

    //===========================================购物车===================================================

    /**
     * 加入购物车
     * @param shopping 加入购物车的商品
     * @return
     */
    int joinTheShoppingCart(UserShoppingTrolley shopping);

    /**
     * 移除购物车
     * @param userId 用户id
     * @param commodityId 商品id
     * @return
     */
    int removeTheShoppingCart(Integer userId,String commodityId);

    /**
     * 分页拉取购物车的商品
     * 按加入时间降序排序
     * @param curPage 当前页
     * @param pageSize 页面大小
     * @param userId
     * @return
     */
    List<Commodity> getTheShoppingCartByPage(Integer userId,long curPage, int pageSize);

    /**
     * 某人购物车的数量
     * @param userId 用户id
     * @return
     */
    long countTheShoppingCart(Integer userId);


    //===========================================订单(userMapper中有实现)===================================================
}
