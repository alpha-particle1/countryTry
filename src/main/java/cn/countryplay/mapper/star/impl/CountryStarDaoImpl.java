package cn.countryplay.mapper.star.impl;

import cn.countryplay.entity.star.CountryStar;
import cn.countryplay.mapper.star.ICountryStarDao;
import com.mongodb.client.result.UpdateResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

/**
 * Author:甲粒子
 * Date: 2022/4/9
 * Description：
 */
@Repository
public class CountryStarDaoImpl implements ICountryStarDao {
    @Autowired
    private MongoTemplate mongo;
    @Override
    public int starCountry(CountryStar countryStar) {
        CountryStar insert = mongo.insert(countryStar);
        if(insert==null)
            return 0;
        return 1;
    }

    @Override
    public int starCountry(int countryId, int score) {
        Query query=new Query();
        query.addCriteria(Criteria.where("countryId").is(countryId));
        CountryStar one = mongo.findOne(query, CountryStar.class);
        if(one == null){
            one = new CountryStar();
            one.setCountryId(countryId);
            one.setScore(score);
            one.setScoreNums(1);
            if(starCountry(one) == 1){
                return 1;
            }
        }else{
            Update update=new Update().inc("score",score).inc("scoreNums",1);
            UpdateResult updateResult = mongo.updateFirst(query, update, CountryStar.class);
            if(updateResult!=null && updateResult.getModifiedCount()!=0){
                return 1;
            }
        }
        return 0;
    }

    @Override
    public float getCountryStar(int countryId) {
        Query query=new Query();
        query.addCriteria(Criteria.where("countryId").is(countryId));
        CountryStar one = mongo.findOne(query, CountryStar.class);
        if(one == null){
                return 0;
        }else{
            int score = one.getScore();
            int scoreNums = one.getScoreNums();
            return score/(float)scoreNums;
        }
    }
}
