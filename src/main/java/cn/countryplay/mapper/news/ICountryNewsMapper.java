package cn.countryplay.mapper.news;

import cn.countryplay.entity.news.News;

import java.util.List;

public interface ICountryNewsMapper {
    /**
     * 将爬取到的新闻放入数据库中
     * @param news 新闻
     * @return
     */
    boolean addNewsfromSprider(News news);

    /**
     * 判断标题为title的新闻是否存在
     * @param title 新闻标题
     * @return
     */
    boolean existedNews(String title);

    /**
     * 新闻总数
     * @return
     */
    long countNews();

    /**
     * 新闻分页
     * @return
     */
    List<News> selectNewsPage(long curPage, int pageSize);
}
