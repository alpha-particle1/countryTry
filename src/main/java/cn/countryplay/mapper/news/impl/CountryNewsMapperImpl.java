package cn.countryplay.mapper.news.impl;

import cn.countryplay.entity.news.News;
import cn.countryplay.mapper.news.ICountryNewsMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Author:甲粒子
 * Date: 2022/3/22
 * Description：
 */
@Repository
public class CountryNewsMapperImpl implements ICountryNewsMapper {

    @Autowired
    private MongoTemplate mongo;

    @Override
    public boolean addNewsfromSprider(News news) {
        News insert = mongo.insert(news);
        if(insert==null) return false;
        return true;
    }

    @Override
    public boolean existedNews(String title) {
        Query query=new Query();
        query.addCriteria(Criteria.where("news_title").is(title));
        List<News> news = mongo.find(query, News.class);
        if(news==null||news.size()==0) return false;
        return true;
    }

    @Override
    public long countNews() {
        return mongo.count(new Query(),News.class);
    }

    @Override
    public List<News> selectNewsPage(long curPage, int pageSize) {
        Query query=new Query();
        Sort sort=Sort.by(Sort.Direction.DESC,"_id");
        return mongo.find(query.with(sort).skip((curPage-1)*pageSize).limit(pageSize),News.class);

    }
}
