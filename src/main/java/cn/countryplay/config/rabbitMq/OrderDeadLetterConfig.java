package cn.countryplay.config.rabbitMq;

import cn.countryplay.entity.constantPool.RabbitConstant;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

/**
 * Author:甲粒子
 * Date: 2022/4/7
 * Description：订单队列的死信队列的配置
 */
@Configuration
public class OrderDeadLetterConfig {

    //订单队列：死信交换机
    @Bean
    TopicExchange deadLetterExchangeOrder() {
        return new TopicExchange(RabbitConstant.DEAD_LETTER_EXCHANGE_ORDER,true,false,null);
    }

    //订单队列：绑定死信交换机
    //true表示持久化该队列
    @Bean
    public Queue orderQueue() {
        Map<String, Object> props = new HashMap<>();
        // 消息的生存时间 12h
        props.put("x-message-ttl", 1000*10/*12*60*60*1000*/);

        // 设置订单队列所关联的死信交换器（当队列消息到期后依然没有消费，则加入死信 队列）
        props.put("x-dead-letter-exchange", RabbitConstant.DEAD_LETTER_EXCHANGE_ORDER);

        // 设置订单队列所关联的死信交换器的routingKey，如果没有特殊指定，使用原队列的routingKey
        // 出现dead letter之后将dead letter重新按照指定的routing-key发送
        props.put("x-dead-letter-routing-key", RabbitConstant.ORDER_DEAD_LETTER_EX_ROUTING_KEY);

       //props.put("x-max-length",1000);

        return new Queue(RabbitConstant.ORDER_QUEUE, true, false, false, props);
    }

    //订单的死信队列
    @Bean
    public Queue deadLetterQueueOrder() {
        return new Queue(RabbitConstant.DEAD_LETTER_QUEUE_ORDER, true, false, false);
    }

    //订单队列：交换机
    //话题交换机
    @Bean
    TopicExchange topicExchangeOrder() {
        return new TopicExchange(RabbitConstant.TOPIC_ORDER_EXCHANGE,true,false,null);
    }


    //绑定订单消息队列-订单消息交换机
    @Bean
    public Binding bindingOrderQ2Ex() {
        return BindingBuilder.bind(orderQueue()).to(topicExchangeOrder()).with(RabbitConstant.ORDER_ROUTING_KEY);
    }

    //绑定订单死信消息队列-订单消息死信交换机
    @Bean
    public Binding bindingOrderDLQ2DLX() {
        return BindingBuilder.bind(deadLetterQueueOrder()).to(deadLetterExchangeOrder()).with(RabbitConstant.DEAD_LETTER_EX_TO_Q_ROUTING_KEY);
    }

/*    @Bean
    public Binding bindingOrderQ2DLX() {
        return BindingBuilder.bind(orderQueue()).to(deadLetterExchangeOrder()).with(RabbitConstant.ORDER_DEAD_LETTER_EX_ROUTING_KEY);
    }*/


}
