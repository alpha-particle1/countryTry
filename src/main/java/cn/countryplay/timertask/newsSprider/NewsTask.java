package cn.countryplay.timertask.newsSprider;

import cn.countryplay.entity.news.News;
import cn.countryplay.service.news.ICountryNewsService;
import cn.countryplay.service.sprider.ISpriderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Author:甲粒子
 * Date: 2022/3/22
 * Description：
 */
@Component
public class NewsTask {
    @Autowired
    private ISpriderService spriderService;
    @Autowired
    private ICountryNewsService countryNewsService;

    @Scheduled(fixedDelay = 1000*60*60*1)//1个小时执行一次  运行时执行一次
    public void saveNews(){
        List<News> news = spriderService.takeNewfromCTD("");
        for (News _news : news) {
            if (!countryNewsService.existedNews(_news.getNews_title())) {
                countryNewsService.addNewsfromSprider(_news);
            }
        }
    }
}
